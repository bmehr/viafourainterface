package com.gatehousemedia.client.viafoura.util;

import com.gatehousemedia.client.viafoura.bean.DateRange;
import com.gatehousemedia.client.viafoura.constant.PropertyValue;
import com.gatehousemedia.client.viafoura.sns.SNSHandler;

public class DirectoryUtils {
	
	public static String exportJsonFileName = null;
	public static String fullJsonFileName = null;
	
	public static synchronized boolean createExportFile(DateRange dateRange){
		String currentTimestamp = System.currentTimeMillis()+"";
		try{
			if(PropertyValue.LOCAL_DIRECTORY == null || PropertyValue.LOCAL_DIRECTORY.trim().length() == 0){
				LoggingUtils.logInfo("Error : Unable to load viafoura.properties. viafourainterface.jar and viafoura.properties should be under the same folder.");
				SNSHandler.sendSimpleMessage("Error : Unable to load viafoura.properties. viafourainterface.jar and viafoura.properties should be under the same folder.", "@ViafouraImport! Failed");
				System.exit(-1);
			}			
			
			exportJsonFileName = currentTimestamp + "_" + dateRange.getFromDate() + "To" + dateRange.getToDate()+".txt";
						
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
			
			return false;
		}
		return true;
	}
	
}
