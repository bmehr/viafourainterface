package com.gatehousemedia.client.viafoura.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggingUtils {
	static final Logger logger = LogManager.getLogger(LoggingUtils.class); 
	private static StringWriter stringWriter = new StringWriter();
	
	public static long getCurrentTimeinMilli(){       
		return System.currentTimeMillis();
	}
	
	public static void logDebug(String text){
		logger.debug(text);
	}
	
	public static void logInfo(String text){
		logger.info(text);
	}
	
	public static void logWarn(String text){
		logger.warn(text);
	}

	public static void logTrace(String text){
		logger.trace(text);
	}

	public static void logError(String text){
		logger.error(text);
	}
	
	@Deprecated
	public static void logEndTimeWithInfo(long startTime, String text){
		long differenceInMilli = System.currentTimeMillis()-startTime;
		
		StringBuffer sb = new StringBuffer(text)
		.append(differenceInMilli).append(" - Time in sec:")
		.append(TimeUnit.MILLISECONDS.toSeconds(differenceInMilli))
		.append(" - Time in minute:")
		.append(TimeUnit.MILLISECONDS.toMinutes(differenceInMilli));
		
		logger.info(sb.toString());
	}
	
	public static void requestEndLog(long startTime, String sbLog){
		requestEndLog(startTime, new StringBuilder(sbLog)); 
	}
	
	public static void requestEndLog(long startTime, StringBuilder sbLog){
		long differenceInMilli = System.currentTimeMillis()-startTime;
		
		sbLog.append(differenceInMilli).append(" - Time in sec:")
		.append(TimeUnit.MILLISECONDS.toSeconds(differenceInMilli))
		.append(" - Time in minute:")
		.append(TimeUnit.MILLISECONDS.toMinutes(differenceInMilli));
		
		logger.info(sbLog.toString());
	}

	public static String getExceptionString(Throwable exceptionStackTrace){		
		try{
			PrintWriter printWriter = new PrintWriter(stringWriter);
			exceptionStackTrace.printStackTrace(printWriter);
		}catch(Exception e){
			e.printStackTrace();
		}
		return stringWriter.toString();
	}
}

