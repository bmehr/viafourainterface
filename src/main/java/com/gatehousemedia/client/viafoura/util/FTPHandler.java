package com.gatehousemedia.client.viafoura.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import com.gatehousemedia.client.viafoura.constant.PropertyValue;


public class FTPHandler {
	private static FTPClient ftp = new FTPClient();
	
	public static boolean transfer(){
		try{
			ftp.connect(PropertyValue.FTPSERVER_ADDRESS);		
			
			if(!ftp.login(PropertyValue.FTPSERVER_USR, PropertyValue.FTPSERVER_PWD))
            {
                ftp.logout();
                return false;
            }
			
            int reply = ftp.getReplyCode();
            //FTPReply stores a set of constants for FTP reply codes.
            if (!FTPReply.isPositiveCompletion(reply))
            {
                ftp.disconnect();
                return false;
            }
            
            ftp.enterLocalPassiveMode();
            ftp.setFileType(FTP.BINARY_FILE_TYPE);
            ftp.changeWorkingDirectory(PropertyValue.FTPSERVER_FOLDERNAME);
            
            InputStream input = null;
            File file = new File(PropertyValue.LOCAL_DIRECTORY +  DirectoryUtils.exportJsonFileName);
            
            System.out.println("File: " +PropertyValue.LOCAL_DIRECTORY +  DirectoryUtils.exportJsonFileName + " isFileExist flag:"+file.exists());
            
            if(file.exists()){
	            input = new FileInputStream(file);
	            ftp.storeFile(DirectoryUtils.exportJsonFileName, input);
	            
	            input.close();
	            file.delete();	            
            }			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}

		return true;
	}

}

