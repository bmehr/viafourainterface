package com.gatehousemedia.client.viafoura.util;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
//import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
//@EnableTransactionManagement
@ComponentScan(basePackages = "com.gatehousemedia.client.viafoura")
public class ViafouraInterfaceConfig {

}
