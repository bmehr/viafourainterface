package com.gatehousemedia.client.viafoura.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;



public class StringUtils {
	
	public static boolean isNullOrEmpty(String s) {
		return ( s == null || s.trim().equals("") );
	}
	
	public static String getStringFromInputStream(InputStream is) {
		StringBuilder sb = new StringBuilder();

		String line;
		try (BufferedReader br = new BufferedReader(new InputStreamReader(is))){
			while ((line = br.readLine()) != null) {
				sb.append(line.trim());
			}
		} catch (IOException e) {
			LoggingUtils.logDebug(LoggingUtils.getExceptionString(e));
		} 
		return sb.toString();
	}
}