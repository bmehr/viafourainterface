package com.gatehousemedia.client.viafoura.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import com.gatehousemedia.client.viafoura.constant.PropertyKey;
import com.gatehousemedia.client.viafoura.constant.PropertyValue;

public class PropertyLoader {
	public static void loadApplicationProperties() {		
		try {
			Properties properties = new Properties();
			boolean insideJar = false;
			
			PropertyValue.APPLICATION_MODE = System.getenv("application_mode");
			System.out.println(".........application-mode from env............... : "+ PropertyValue.APPLICATION_MODE);
			
			String filePathFromJar = PropertyLoader.class.getProtectionDomain().getCodeSource().getLocation().getPath();	
			
			System.out.println("Jar file path:" + filePathFromJar);
			
			String filePathFromJarWithoutName = "";
			
			if(filePathFromJar.endsWith("jar")){
				insideJar = true;
				filePathFromJarWithoutName = filePathFromJar.substring(0, filePathFromJar.lastIndexOf("/"));
			}else{
				filePathFromJarWithoutName = filePathFromJar+ "viafoura.properties";
			}			
			
			if(insideJar){
				filePathFromJarWithoutName = filePathFromJarWithoutName + "/" + "viafoura.properties";
			}			
			
			System.out.println("removed the .jar file name and added property file name : "+filePathFromJarWithoutName);
			
			
			String currentOS = System.getProperty("os.name").toLowerCase();
			PropertyValue.OS = currentOS;
			
			System.out.println("currentOS : "+currentOS);
			
			if(currentOS.contains("window")){
				filePathFromJarWithoutName = filePathFromJarWithoutName.substring(1, filePathFromJarWithoutName.length());
			}
			
			System.out.println("For windows only path should not start from / : " + filePathFromJarWithoutName);
			
			final Path pathFile = Paths.get(filePathFromJarWithoutName);
			
			if("prod".equalsIgnoreCase(PropertyValue.APPLICATION_MODE)){
				System.out.println("Going to load property file from local linux,ubuntu prod machine...");
				
				   properties.load(new FileInputStream("/opt/viafoura/viafoura.properties"));
			}else{
				
				if (Files.exists(pathFile, LinkOption.NOFOLLOW_LINKS)) {
					
					System.out.println("Going to load property file from local directory...");
					
				   properties.load(new FileInputStream(filePathFromJarWithoutName));
				   
				 }else {
					 
					filePathFromJarWithoutName = "viafoura.properties";
					System.out.println("Cant load property file from local directory, now trying to load property file from the classpath : "+filePathFromJarWithoutName);
					 
					InputStream inputStream = PropertyLoader.class.getClassLoader().getResourceAsStream(filePathFromJarWithoutName);
					if (inputStream == null) {
						
						System.out.println("ResourceAsStream failed, now will try to load using SystemResource..");
						
						URL url = ClassLoader.getSystemResource(filePathFromJarWithoutName);
						
						System.out.println("url:"+url);
						
						if (url != null) {
							inputStream = url.openStream();
						}
					}	
					
				    properties.load(inputStream);			      
				 }
			}
			
			PropertyValue.USE_FTP = properties.getProperty(PropertyKey.USE_FTP);
			PropertyValue.FTPSERVER_ADDRESS = properties.getProperty(PropertyKey.FTPSERVER_ADDRESS);
			PropertyValue.FTPSERVER_USR = properties.getProperty(PropertyKey.FTPSERVER_USR);
			PropertyValue.FTPSERVER_PWD = properties.getProperty(PropertyKey.FTPSERVER_PWD);			
			PropertyValue.LOCAL_DIRECTORY = properties.getProperty(PropertyKey.LOCAL_DIRECTORY);
			
			if(currentOS.contains("window")){
				if(PropertyValue.LOCAL_DIRECTORY == null || PropertyValue.LOCAL_DIRECTORY.trim().length() == 0){
					PropertyValue.LOCAL_DIRECTORY = "c:\\temp\\viafoura\\";
				}
				
				System.setProperty("log4j.configurationFile", "log4j2_win.xml");
			}else{
				/*
				 * For all other operating systems - other than Windows
				 */
				if(PropertyValue.LOCAL_DIRECTORY == null || PropertyValue.LOCAL_DIRECTORY.trim().length() == 0){
					PropertyValue.LOCAL_DIRECTORY = "/opt/viafoura/";
				}
				
				System.setProperty("log4j.configurationFile", "log4j2.xml");
				

			}			
			
			if(PropertyValue.APPLICATION_MODE == null || "".equals(PropertyValue.APPLICATION_MODE)){
				PropertyValue.APPLICATION_MODE = properties.getProperty(PropertyKey.APPLICATION_MODE);
				
				System.out.println("....................APPLICATION_MODE from properties....................: "+ PropertyValue.APPLICATION_MODE);
				LoggingUtils.logInfo("....................APPLICATION_MODE from properties....................: "+ PropertyValue.APPLICATION_MODE);
			}
			

			if("dev".equalsIgnoreCase(PropertyValue.APPLICATION_MODE)){
				PropertyValue.AWS_SNS_ARN =  properties.getProperty(PropertyKey.AWS_SNS_ARN_DEV);
				
				PropertyValue.DB_URL = properties.getProperty(PropertyKey.DB_URL_DEV);
				PropertyValue.DB_USR = properties.getProperty(PropertyKey.DB_USR_DEV);
				PropertyValue.DB_PWD = properties.getProperty(PropertyKey.DB_PWD_DEV);
				
				PropertyValue.FTPSERVER_FOLDERNAME = properties.getProperty(PropertyKey.FTPSERVER_FOLDERNAME_DEV);
				
			}else{
				/*
				 * Production
				 */
				PropertyValue.AWS_SNS_ARN =  properties.getProperty(PropertyKey.AWS_SNS_ARN);		
				
				PropertyValue.DB_URL = properties.getProperty(PropertyKey.DB_URL);
				PropertyValue.DB_USR = properties.getProperty(PropertyKey.DB_USR);
				PropertyValue.DB_PWD = properties.getProperty(PropertyKey.DB_PWD);
				
				PropertyValue.FTPSERVER_FOLDERNAME = properties.getProperty(PropertyKey.FTPSERVER_FOLDERNAME);
			}
			
			PropertyValue.CLIENT_ID = properties.getProperty(PropertyKey.CLIENT_ID);
			PropertyValue.CLIENT_SECRET = properties.getProperty(PropertyKey.CLIENT_SECRET);
			PropertyValue.USE_OVERALL_GATEHOUSE_AGGREGATE_SCOPE = properties.getProperty(PropertyKey.USE_OVERALL_GATEHOUSE_AGGREGATE_SCOPE);	
			PropertyValue.CRON_EXPRESSION = properties.getProperty(PropertyKey.CRON_EXPRESSION);
			
		    PropertyValue.AWS_ACCESSKEY_ID =  properties.getProperty(PropertyKey.AWS_ACCESSKEY_ID); 
		    PropertyValue.AWS_SECRET_ACCESS_KEY =  properties.getProperty(PropertyKey.AWS_SECRET_ACCESS_KEY);
		    
		    System.out.println();
		    System.out.println("PropertyValue.APPLICATION_MODE : "+ PropertyValue.APPLICATION_MODE);
		    System.out.println("PropertyValue.OS : "+ PropertyValue.OS);
		    System.out.println("PropertyValue.USE_FTP : "+ PropertyValue.USE_FTP);
		    System.out.println("PropertyValue.FTPSERVER_ADDRESS : "+ PropertyValue.FTPSERVER_ADDRESS);
		    System.out.println("PropertyValue.FTPSERVER_FOLDERNAME : "+ PropertyValue.FTPSERVER_FOLDERNAME);
		    System.out.println("PropertyValue.LOCAL_DIRECTORY : "+ PropertyValue.LOCAL_DIRECTORY);
		    System.out.println("PropertyValue.AWS_SNS_ARN : "+ PropertyValue.AWS_SNS_ARN);
		    System.out.println("PropertyValue.DB_URL : "+ PropertyValue.DB_URL);		    
		    System.out.println("PropertyValue.USE_OVERALL_GATEHOUSE_AGGREGATE_SCOPE : "+ PropertyValue.USE_OVERALL_GATEHOUSE_AGGREGATE_SCOPE);
		    System.out.println("PropertyValue.CRON_EXPRESSION : "+ PropertyValue.CRON_EXPRESSION);
		    System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
