package com.gatehousemedia.client.viafoura.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;

public class ViaFouraClientTest {

	public static void main(String[] args) {
		try{
			String scope = "00000000-0000-4000-8000-06d97e804172";
			String uri = "/users/topics/unsubscribed";
			
			String serviceURL = "https://data.viafoura.io/data/v1/"+scope+uri+"?from=1530421260001&to=1531799999999";		
			
			System.out.println(serviceURL);
			
			
			OAuth2RestTemplate oauth2RestTemplate = getRestTemplate(scope);
			
			String responseJson = oauth2RestTemplate.getForObject(serviceURL, String.class);
			
			System.out.println("responseJson: "+ responseJson);
			

			
		}catch(Exception e){
			System.out.println("111111111111111111111111");
			e.printStackTrace();
		}

	}
	
	private static OAuth2RestTemplate getRestTemplate(String scope) {
		ClientCredentialsResourceDetails resourceDetails = null;
		try{
			resourceDetails = new ClientCredentialsResourceDetails();
			resourceDetails.setGrantType("client_credentials");
			resourceDetails.setAccessTokenUri("https://auth.viafoura.io/authorize_client");
		
			//-- set the clients info
			resourceDetails.setClientId("f72a57ce-43a4-40ee-a145-31cce7f8711e"); // cllient_id
			resourceDetails.setClientSecret("l1AC%SS6Nh4tb6NbaVzl@AnIA_qll6"); //secret-access-key
			
			// set scopes
			List<String> scopes = new ArrayList<>();
			scopes.add(scope); 
			resourceDetails.setScope(scopes);
			
			return new OAuth2RestTemplate(resourceDetails);				
		}catch(Exception e){
			System.out.println("222222222222222");
			e.printStackTrace();
		}
		
		return null;
	}

}
