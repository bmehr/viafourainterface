package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.UVisits;

@Repository(value="uVisitsRepo")
public interface UVisitsRepo extends CrudRepository<UVisits, Integer>{

	@Query("SELECT di FROM UVisits di WHERE di.user = (:user) order by di.scope")
	public List<UVisits> findByUser(@Param("user") String user);
}
