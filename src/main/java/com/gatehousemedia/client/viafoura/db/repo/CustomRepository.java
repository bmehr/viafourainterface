package com.gatehousemedia.client.viafoura.db.repo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component(value="customRepository")
public class CustomRepository {

	@PersistenceContext
    private EntityManager entityManager; 
	
	@Transactional
	public void registeredDistinct(){
		Query queryCount = entityManager.createNativeQuery(
				"insert into viafoura.registered_distinct (uuid) SELECT distinct uuid FROM viafoura.u_registered" );
		queryCount.executeUpdate();
	}
	
	@Transactional
	public void truncaleAllViaFouraData(){
		Query queryCount = entityManager.createNativeQuery(
				"truncate viafoura.e_anonymous_total" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.e_browser_clicked" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.e_browser_received" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.e_clicks_total" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.e_js_total" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.e_opens_total" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.e_visitors_attention" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.e_visitors_user_agents" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.e_visitors_visits" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.u_anonymous_total" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.u_comments" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.u_dislikes" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.u_follows_pages" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.u_follows_users" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.u_likes" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.u_logins" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.u_ratings" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.u_registered" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.u_registered_total" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.u_schedule" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.u_topics_subscribed" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.u_topics_unsubscribed" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.u_visits" );
		queryCount.executeUpdate();
		
		queryCount = entityManager.createNativeQuery(
				"truncate viafoura.registered_distinct" );
		queryCount.executeUpdate();
		
	}
    

}
