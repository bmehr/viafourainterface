/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehousemedia.client.viafoura.db.domain;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "e_js_total")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EJsTotal.findAll", query = "SELECT e FROM EJsTotal e")
    , @NamedQuery(name = "EJsTotal.findById", query = "SELECT e FROM EJsTotal e WHERE e.id = :id")
    , @NamedQuery(name = "EJsTotal.findByScope", query = "SELECT e FROM EJsTotal e WHERE e.scope = :scope")
    , @NamedQuery(name = "EJsTotal.findByAggregate", query = "SELECT e FROM EJsTotal e WHERE e.aggregate = :aggregate")})
public class EJsTotal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 185)
    @Column(name = "scope")
    private String scope;
    @Column(name = "aggregate")
    private BigInteger aggregate;

    public EJsTotal() {
    }

    public EJsTotal(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public BigInteger getAggregate() {
        return aggregate;
    }

    public void setAggregate(BigInteger aggregate) {
        this.aggregate = aggregate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EJsTotal)) {
            return false;
        }
        EJsTotal other = (EJsTotal) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehousemedia.client.viafoura.db.domain.EJsTotal[ id=" + id + " ]";
    }
    
}
