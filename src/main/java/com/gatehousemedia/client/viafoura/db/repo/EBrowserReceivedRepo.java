package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.EBrowserReceived;

@Repository(value="eBrowserReceivedRepo")
public interface EBrowserReceivedRepo extends CrudRepository<EBrowserReceived, Integer>{
	
	@Query("SELECT di FROM EBrowserReceived di WHERE di.uuid = (:uuid) order by di.scope")
	public List<EBrowserReceived> findByUser(@Param("uuid") String uuid);	
}
