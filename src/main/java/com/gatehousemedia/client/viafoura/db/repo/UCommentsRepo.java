package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.UComments;

@Repository(value="uCommentsRepo")
public interface UCommentsRepo extends CrudRepository<UComments, Integer>{

	@Query("SELECT di FROM UComments di WHERE di.user = (:user) order by di.scope")
	public List<UComments> findByUser(@Param("user") String user);
}
