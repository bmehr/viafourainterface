package com.gatehousemedia.client.viafoura.db.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.EJsTotal;


@Repository(value="eJsTotalRepo")
public interface EJsTotalRepo extends CrudRepository<EJsTotal, Integer>{

}
