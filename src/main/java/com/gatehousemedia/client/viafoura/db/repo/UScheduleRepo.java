package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.USchedule;


@Repository(value="uScheduleRepo")
public interface UScheduleRepo extends CrudRepository<USchedule, Integer>{

	@Query("SELECT di FROM USchedule di WHERE di.user = (:user) order by di.scope")
	public List<USchedule> findByUser(@Param("user") String user);
}
