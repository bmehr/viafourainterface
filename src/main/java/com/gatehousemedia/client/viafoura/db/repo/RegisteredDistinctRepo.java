package com.gatehousemedia.client.viafoura.db.repo;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.RegisteredDistinct;

@Repository(value="registeredDistinctRepo")
public interface RegisteredDistinctRepo extends PagingAndSortingRepository<RegisteredDistinct, Integer>{

}
