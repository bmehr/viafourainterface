/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehousemedia.client.viafoura.db.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "e_visitors_visits")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EVisitorsVisits.findAll", query = "SELECT e FROM EVisitorsVisits e")
    , @NamedQuery(name = "EVisitorsVisits.findById", query = "SELECT e FROM EVisitorsVisits e WHERE e.id = :id")
    , @NamedQuery(name = "EVisitorsVisits.findByScope", query = "SELECT e FROM EVisitorsVisits e WHERE e.scope = :scope")
    , @NamedQuery(name = "EVisitorsVisits.findByUuid", query = "SELECT e FROM EVisitorsVisits e WHERE e.uuid = :uuid")})
public class EVisitorsVisits implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 185)
    @Column(name = "scope")
    private String scope;
    @Size(max = 185)
    @Column(name = "uuid")
    private String uuid;
    @Lob
    @Column(name = "visits")
    private byte[] visits;

    public EVisitorsVisits() {
    }

    public EVisitorsVisits(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public byte[] getVisits() {
        return visits;
    }

    public void setVisits(byte[] visits) {
        this.visits = visits;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EVisitorsVisits)) {
            return false;
        }
        EVisitorsVisits other = (EVisitorsVisits) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehousemedia.client.viafoura.db.domain.EVisitorsVisits[ id=" + id + " ]";
    }
    
}
