/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehousemedia.client.viafoura.db.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "registered_distinct")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RegisteredDistinct.findAll", query = "SELECT r FROM RegisteredDistinct r")
    , @NamedQuery(name = "RegisteredDistinct.findById", query = "SELECT r FROM RegisteredDistinct r WHERE r.id = :id")
    , @NamedQuery(name = "RegisteredDistinct.findByUuid", query = "SELECT r FROM RegisteredDistinct r WHERE r.uuid = :uuid")})
public class RegisteredDistinct implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 185)
    @Column(name = "uuid")
    private String uuid;

    public RegisteredDistinct() {
    }

    public RegisteredDistinct(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegisteredDistinct)) {
            return false;
        }
        RegisteredDistinct other = (RegisteredDistinct) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehousemedia.client.viafoura.db.domain.RegisteredDistinct[ id=" + id + " ]";
    }
    
}
