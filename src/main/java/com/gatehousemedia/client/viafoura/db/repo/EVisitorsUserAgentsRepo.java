package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.EVisitorsUserAgents;

@Repository(value="eVisitorsUserAgentsRepo")
public interface EVisitorsUserAgentsRepo extends CrudRepository<EVisitorsUserAgents, Integer>{

	@Query("SELECT di FROM EVisitorsUserAgents di WHERE di.user = (:user) order by di.scope")
	public List<EVisitorsUserAgents> findByUser(@Param("user") String user);
}
