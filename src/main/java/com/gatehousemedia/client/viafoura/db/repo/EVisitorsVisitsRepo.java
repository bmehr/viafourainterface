package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.EVisitorsVisits;

@Repository(value="eVisitorsVisitsRepo")
public interface EVisitorsVisitsRepo extends CrudRepository<EVisitorsVisits, Integer>{

	@Query("SELECT di FROM EVisitorsVisits di WHERE di.uuid = (:uuid) order by di.scope")
	public List<EVisitorsVisits> findByUser(@Param("uuid") String uuid);
}
