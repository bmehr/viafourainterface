package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.EBrowserClicked;


@Repository(value="eBrowserClickedRepo")
public interface EBrowserClickedRepo extends CrudRepository<EBrowserClicked, Integer>{

	@Query("SELECT di FROM EBrowserClicked di WHERE di.uuid = (:uuid) order by di.scope")
	public List<EBrowserClicked> findByUser(@Param("uuid") String uuid);	
	
}
