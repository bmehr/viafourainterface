package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.UFollowsUsers;


@Repository(value="uFollowsUsersRepo")
public interface UFollowsUsersRepo extends CrudRepository<UFollowsUsers, Integer>{

	
	@Query("SELECT di FROM UFollowsUsers di WHERE di.user = (:user) order by di.scope")
	public List<UFollowsUsers> findByUser(@Param("user") String user);
}
