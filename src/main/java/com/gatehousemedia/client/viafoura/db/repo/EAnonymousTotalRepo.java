package com.gatehousemedia.client.viafoura.db.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.EAnonymousTotal;



@Repository(value="eAnonymousTotalRepo")
public interface EAnonymousTotalRepo extends CrudRepository<EAnonymousTotal, Integer> {

}
