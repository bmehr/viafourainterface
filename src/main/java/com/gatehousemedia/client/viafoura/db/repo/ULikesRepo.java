package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.ULikes;


@Repository(value="uLikesRepo")
public interface ULikesRepo extends CrudRepository<ULikes, Integer>{

	@Query("SELECT di FROM ULikes di WHERE di.user = (:user) order by di.scope")
	public List<ULikes> findByUser(@Param("user") String user);
}
