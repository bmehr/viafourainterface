/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehousemedia.client.viafoura.db.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "e_visitors_user_agents")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EVisitorsUserAgents.findAll", query = "SELECT e FROM EVisitorsUserAgents e")
    , @NamedQuery(name = "EVisitorsUserAgents.findById", query = "SELECT e FROM EVisitorsUserAgents e WHERE e.id = :id")
    , @NamedQuery(name = "EVisitorsUserAgents.findByScope", query = "SELECT e FROM EVisitorsUserAgents e WHERE e.scope = :scope")
    , @NamedQuery(name = "EVisitorsUserAgents.findByUser", query = "SELECT e FROM EVisitorsUserAgents e WHERE e.user = :user")})
public class EVisitorsUserAgents implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 185)
    @Column(name = "scope")
    private String scope;
    @Size(max = 185)
    @Column(name = "user")
    private String user;
    @Lob
    @Column(name = "user_agents")
    private byte[] userAgents;

    public EVisitorsUserAgents() {
    }

    public EVisitorsUserAgents(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public byte[] getUserAgents() {
        return userAgents;
    }

    public void setUserAgents(byte[] userAgents) {
        this.userAgents = userAgents;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EVisitorsUserAgents)) {
            return false;
        }
        EVisitorsUserAgents other = (EVisitorsUserAgents) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehousemedia.client.viafoura.db.domain.EVisitorsUserAgents[ id=" + id + " ]";
    }
    
}
