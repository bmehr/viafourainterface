package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.URegistered;

@Repository(value="uRegisteredRepo")
public interface URegisteredRepo extends CrudRepository<URegistered, Integer>{

	@Query("SELECT di FROM URegistered di WHERE di.uuid = (:uuid) order by di.scope")
	public List<URegistered> findByUser(@Param("uuid") String uuid);
}
