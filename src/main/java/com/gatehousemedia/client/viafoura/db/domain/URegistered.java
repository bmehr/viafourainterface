/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehousemedia.client.viafoura.db.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "u_registered")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "URegistered.findAll", query = "SELECT u FROM URegistered u")
    , @NamedQuery(name = "URegistered.findById", query = "SELECT u FROM URegistered u WHERE u.id = :id")
    , @NamedQuery(name = "URegistered.findByScope", query = "SELECT u FROM URegistered u WHERE u.scope = :scope")
    , @NamedQuery(name = "URegistered.findByUuid", query = "SELECT u FROM URegistered u WHERE u.uuid = :uuid")
    , @NamedQuery(name = "URegistered.findByName", query = "SELECT u FROM URegistered u WHERE u.name = :name")
    , @NamedQuery(name = "URegistered.findByEmail", query = "SELECT u FROM URegistered u WHERE u.email = :email")})
public class URegistered implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 185)
    @Column(name = "scope")
    private String scope;
    @Size(max = 185)
    @Column(name = "uuid")
    private String uuid;
    @Size(max = 450)
    @Column(name = "name")
    private String name;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 450)
    @Column(name = "email")
    private String email;
    @Lob
    @Column(name = "social")
    private byte[] social;

    public URegistered() {
    }

    public URegistered(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte[] getSocial() {
        return social;
    }

    public void setSocial(byte[] social) {
        this.social = social;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof URegistered)) {
            return false;
        }
        URegistered other = (URegistered) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehousemedia.client.viafoura.db.domain.URegistered[ id=" + id + " ]";
    }
    
}
