package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.UTopicsUnsubscribed;


@Repository(value="uTopicsUnsubscribedRepo")
public interface UTopicsUnsubscribedRepo extends CrudRepository<UTopicsUnsubscribed, Integer>{

	@Query("SELECT di FROM UTopicsUnsubscribed di WHERE di.user = (:user) order by di.scope")
	public List<UTopicsUnsubscribed> findByUser(@Param("user") String user);
}
