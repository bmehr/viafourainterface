package com.gatehousemedia.client.viafoura.db.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.URegisteredTotal;


@Repository(value="uRegisteredTotalRepo")
public interface URegisteredTotalRepo extends CrudRepository<URegisteredTotal, Integer>{

}
