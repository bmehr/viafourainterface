package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.URatings;

@Repository(value="uRatingsRepo")
public interface URatingsRepo extends CrudRepository<URatings, Integer>{

	@Query("SELECT di FROM URatings di WHERE di.user = (:user) order by di.scope")
	public List<URatings> findByUser(@Param("user") String user);
}
