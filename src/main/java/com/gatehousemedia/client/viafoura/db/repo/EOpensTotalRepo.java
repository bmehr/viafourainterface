package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.EOpensTotal;


@Repository(value="eOpensTotalRepo")
public interface EOpensTotalRepo extends CrudRepository<EOpensTotal, Integer>{

	
	@Query("SELECT di FROM EOpensTotal di WHERE di.user = (:user) order by di.scope")
	public List<EOpensTotal> findByUser(@Param("user") String user);
}
