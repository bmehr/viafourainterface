/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehousemedia.client.viafoura.db.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "u_schedule")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "USchedule.findAll", query = "SELECT u FROM USchedule u")
    , @NamedQuery(name = "USchedule.findById", query = "SELECT u FROM USchedule u WHERE u.id = :id")
    , @NamedQuery(name = "USchedule.findByScope", query = "SELECT u FROM USchedule u WHERE u.scope = :scope")
    , @NamedQuery(name = "USchedule.findByUser", query = "SELECT u FROM USchedule u WHERE u.user = :user")})
public class USchedule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 185)
    @Column(name = "scope")
    private String scope;
    @Size(max = 185)
    @Column(name = "user")
    private String user;
    @Lob
    @Column(name = "view_timestamps")
    private byte[] viewTimestamps;

    public USchedule() {
    }

    public USchedule(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public byte[] getViewTimestamps() {
        return viewTimestamps;
    }

    public void setViewTimestamps(byte[] viewTimestamps) {
        this.viewTimestamps = viewTimestamps;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof USchedule)) {
            return false;
        }
        USchedule other = (USchedule) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehousemedia.client.viafoura.db.domain.USchedule[ id=" + id + " ]";
    }
    
}
