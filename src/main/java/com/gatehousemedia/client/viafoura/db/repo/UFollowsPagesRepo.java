package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.UFollowsPages;

@Repository(value="uFollowsPagesRepo")
public interface UFollowsPagesRepo extends CrudRepository<UFollowsPages, Integer>{

	@Query("SELECT di FROM UFollowsPages di WHERE di.user = (:user) order by di.scope")
	public List<UFollowsPages> findByUser(@Param("user") String user);
}
