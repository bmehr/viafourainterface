/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehousemedia.client.viafoura.db.domain;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "e_browser_clicked")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EBrowserClicked.findAll", query = "SELECT e FROM EBrowserClicked e")
    , @NamedQuery(name = "EBrowserClicked.findById", query = "SELECT e FROM EBrowserClicked e WHERE e.id = :id")
    , @NamedQuery(name = "EBrowserClicked.findByScope", query = "SELECT e FROM EBrowserClicked e WHERE e.scope = :scope")
    , @NamedQuery(name = "EBrowserClicked.findByIsRegistered", query = "SELECT e FROM EBrowserClicked e WHERE e.isRegistered = :isRegistered")
    , @NamedQuery(name = "EBrowserClicked.findByUuid", query = "SELECT e FROM EBrowserClicked e WHERE e.uuid = :uuid")
    , @NamedQuery(name = "EBrowserClicked.findByBroadcast", query = "SELECT e FROM EBrowserClicked e WHERE e.broadcast = :broadcast")
    , @NamedQuery(name = "EBrowserClicked.findByReply", query = "SELECT e FROM EBrowserClicked e WHERE e.reply = :reply")
    , @NamedQuery(name = "EBrowserClicked.findByTopic", query = "SELECT e FROM EBrowserClicked e WHERE e.topic = :topic")})
public class EBrowserClicked implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 185)
    @Column(name = "scope")
    private String scope;
    @Column(name = "is_registered")
    private Boolean isRegistered;
    @Size(max = 185)
    @Column(name = "uuid")
    private String uuid;
    @Column(name = "broadcast")
    private BigInteger broadcast;
    @Column(name = "reply")
    private BigInteger reply;
    @Column(name = "topic")
    private BigInteger topic;

    public EBrowserClicked() {
    }

    public EBrowserClicked(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public Boolean getIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(Boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public BigInteger getBroadcast() {
        return broadcast;
    }

    public void setBroadcast(BigInteger broadcast) {
        this.broadcast = broadcast;
    }

    public BigInteger getReply() {
        return reply;
    }

    public void setReply(BigInteger reply) {
        this.reply = reply;
    }

    public BigInteger getTopic() {
        return topic;
    }

    public void setTopic(BigInteger topic) {
        this.topic = topic;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EBrowserClicked)) {
            return false;
        }
        EBrowserClicked other = (EBrowserClicked) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehousemedia.client.viafoura.db.domain.EBrowserClicked[ id=" + id + " ]";
    }
    
}
