package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.ULogins;


@Repository(value="uLoginsRepo")
public interface ULoginsRepo extends CrudRepository<ULogins, Integer>{

	@Query("SELECT di FROM ULogins di WHERE di.user = (:user) order by di.scope")
	public List<ULogins> findByUser(@Param("user") String user);
}
