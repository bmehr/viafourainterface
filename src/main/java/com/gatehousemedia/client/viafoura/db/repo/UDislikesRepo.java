package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.gatehousemedia.client.viafoura.db.domain.UDislikes;

@Repository(value="uDislikesRepo")
public interface UDislikesRepo extends CrudRepository<UDislikes, Integer>{

	@Query("SELECT di FROM UDislikes di WHERE di.user = (:user) order by di.scope")
	public List<UDislikes> findByUser(@Param("user") String user);
}
