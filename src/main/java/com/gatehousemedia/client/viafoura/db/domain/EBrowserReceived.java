/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehousemedia.client.viafoura.db.domain;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "e_browser_received")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EBrowserReceived.findAll", query = "SELECT e FROM EBrowserReceived e")
    , @NamedQuery(name = "EBrowserReceived.findById", query = "SELECT e FROM EBrowserReceived e WHERE e.id = :id")
    , @NamedQuery(name = "EBrowserReceived.findByScope", query = "SELECT e FROM EBrowserReceived e WHERE e.scope = :scope")
    , @NamedQuery(name = "EBrowserReceived.findByUuid", query = "SELECT e FROM EBrowserReceived e WHERE e.uuid = :uuid")
    , @NamedQuery(name = "EBrowserReceived.findByIsRegistered", query = "SELECT e FROM EBrowserReceived e WHERE e.isRegistered = :isRegistered")
    , @NamedQuery(name = "EBrowserReceived.findByBroadcast", query = "SELECT e FROM EBrowserReceived e WHERE e.broadcast = :broadcast")
    , @NamedQuery(name = "EBrowserReceived.findByReply", query = "SELECT e FROM EBrowserReceived e WHERE e.reply = :reply")
    , @NamedQuery(name = "EBrowserReceived.findByTopic", query = "SELECT e FROM EBrowserReceived e WHERE e.topic = :topic")})
public class EBrowserReceived implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 185)
    @Column(name = "scope")
    private String scope;
    @Size(max = 185)
    @Column(name = "uuid")
    private String uuid;
    @Column(name = "is_registered")
    private Boolean isRegistered;
    @Column(name = "broadcast")
    private BigInteger broadcast;
    @Column(name = "reply")
    private BigInteger reply;
    @Column(name = "topic")
    private BigInteger topic;

    public EBrowserReceived() {
    }

    public EBrowserReceived(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Boolean getIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(Boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

    public BigInteger getBroadcast() {
        return broadcast;
    }

    public void setBroadcast(BigInteger broadcast) {
        this.broadcast = broadcast;
    }

    public BigInteger getReply() {
        return reply;
    }

    public void setReply(BigInteger reply) {
        this.reply = reply;
    }

    public BigInteger getTopic() {
        return topic;
    }

    public void setTopic(BigInteger topic) {
        this.topic = topic;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EBrowserReceived)) {
            return false;
        }
        EBrowserReceived other = (EBrowserReceived) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehousemedia.client.viafoura.db.domain.EBrowserReceived[ id=" + id + " ]";
    }
    
}
