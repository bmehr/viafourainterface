package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.UTopicsSubscribed;


@Repository(value="uTopicsSubscribedRepo")
public interface UTopicsSubscribedRepo extends CrudRepository<UTopicsSubscribed, Integer>{

	@Query("SELECT di FROM UTopicsSubscribed di WHERE di.user = (:user) order by di.scope")
	public List<UTopicsSubscribed> findByUser(@Param("user") String user);
}
