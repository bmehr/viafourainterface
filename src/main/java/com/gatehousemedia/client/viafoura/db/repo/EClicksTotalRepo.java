package com.gatehousemedia.client.viafoura.db.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehousemedia.client.viafoura.db.domain.EClicksTotal;

@Repository(value="eClicksTotalRepo")
public interface EClicksTotalRepo extends CrudRepository<EClicksTotal, Integer>{
	
	@Query("SELECT di FROM EClicksTotal di WHERE di.user = (:user) order by di.scope")
	public List<EClicksTotal> findByUser(@Param("user") String user);
}
