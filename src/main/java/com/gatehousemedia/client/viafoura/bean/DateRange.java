package com.gatehousemedia.client.viafoura.bean;

public class DateRange {
	private String fromDate;
	private String toDate;
	
	private String fromDateTimestamp;
	private String toDateTimestamp;
	
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getFromDateTimestamp() {
		return fromDateTimestamp;
	}
	public void setFromDateTimestamp(String fromDateTimestamp) {
		this.fromDateTimestamp = fromDateTimestamp;
	}
	public String getToDateTimestamp() {
		return toDateTimestamp;
	}
	public void setToDateTimestamp(String toDateTimestamp) {
		this.toDateTimestamp = toDateTimestamp;
	}
}
