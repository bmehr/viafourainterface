package com.gatehousemedia.client.viafoura.bean;

import java.time.LocalDate;

public class WeekDays {
	private LocalDate startDateLocal;
	private LocalDate endDateLocal;
	
	public LocalDate getStartDateLocal() {
		return startDateLocal;
	}
	public void setStartDateLocal(LocalDate startDateLocal) {
		this.startDateLocal = startDateLocal;
	}
	public LocalDate getEndDateLocal() {
		return endDateLocal;
	}
	public void setEndDateLocal(LocalDate endDateLocal) {
		this.endDateLocal = endDateLocal;
	}	
	
}
