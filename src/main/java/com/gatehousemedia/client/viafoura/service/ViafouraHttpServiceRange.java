package com.gatehousemedia.client.viafoura.service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.stereotype.Component;

import com.gatehousemedia.client.viafoura.ViafouraJsonGenerator;
import com.gatehousemedia.client.viafoura.bean.DateRange;
import com.gatehousemedia.client.viafoura.constant.APIURIEnum;
import com.gatehousemedia.client.viafoura.constant.ApplicationConstants;
import com.gatehousemedia.client.viafoura.constant.DomainScopeEnum;
import com.gatehousemedia.client.viafoura.constant.PropertyValue;
import com.gatehousemedia.client.viafoura.db.repo.CustomRepository;
import com.gatehousemedia.client.viafoura.sns.SNSHandler;
import com.gatehousemedia.client.viafoura.util.CommonUtils;
import com.gatehousemedia.client.viafoura.util.DirectoryUtils;
import com.gatehousemedia.client.viafoura.util.FTPHandler;
import com.gatehousemedia.client.viafoura.util.LoggingUtils;
 
@Component(value = "viafouraHttpServiceRange")
public class ViafouraHttpServiceRange {
	
	@Autowired
	public PersistenceService persistenceService;
	
	@Autowired
	public CustomRepository customRepository;
		
	private static final String baseURL = "https://data.viafoura.io/data/v1/"; 
	
	
	@Autowired
	protected ViafouraJsonGenerator viafouraJsonGenerator;

	public void run() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
		long fileProcessedStartTime = System.currentTimeMillis();
		
		LocalDate fromDateLocal = null;
		LocalDate toDateLocal = null;

		String startDate = "07-01-2018";
		String endDate = "08-31-2018";
		
		fromDateLocal = LocalDate.parse(startDate, formatter);
		toDateLocal = LocalDate.parse(endDate, formatter);				
		
		DateRange dateRange = new DateRange();
		//file name
		dateRange.setFromDate( fromDateLocal.getMonthValue() + "-" + fromDateLocal.getDayOfMonth() + "-"+ fromDateLocal.getYear());
		dateRange.setToDate( toDateLocal.getMonthValue() + "-" + toDateLocal.getDayOfMonth()+ "-"+ toDateLocal.getYear());
		//url timestamp
		dateRange.setFromDateTimestamp(CommonUtils.getFromDateString(fromDateLocal.getYear(), fromDateLocal.getMonthValue(), fromDateLocal.getDayOfMonth())+"");
		dateRange.setToDateTimestamp(CommonUtils.getToDateString(toDateLocal.getYear(), toDateLocal.getMonthValue(), toDateLocal.getDayOfMonth())+"");				
		
		
		boolean successful = DirectoryUtils.createExportFile(dateRange);
		if(successful){		
			customRepository.truncaleAllViaFouraData();
			
			/*
			 * calling viafoura
			 */
			callViafouraServices(dateRange);	

			customRepository.registeredDistinct();
		}
		
		try{
			viafouraJsonGenerator.run();
			
			//transferring files from local machine to the ftp
			if("yes".equalsIgnoreCase(PropertyValue.USE_FTP)){
				FTPHandler.transfer();
			}
		}catch(Exception e){
			e.printStackTrace();
			String exceptionStr = LoggingUtils.getExceptionString(e);
			StringBuilder sbError = new StringBuilder("Not able to process for following month ! ")
					.append(" \n  ExportJsonFileName : ")
					.append(DirectoryUtils.exportJsonFileName)
					.append(" \n ");
			LoggingUtils.logInfo(exceptionStr);				
			
			SNSHandler.sendSimpleMessage(sbError.toString(), "@ViafouraImport! Failed");
		}
		
		StringBuilder sbMsg = new StringBuilder("Successfully processed file name : ")
				.append(DirectoryUtils.exportJsonFileName)
				.append(" \n\n ");
		
		long differenceInMilli = System.currentTimeMillis()-fileProcessedStartTime;
		sbMsg
		.append(" -minutes:")
		.append(TimeUnit.MILLISECONDS.toMinutes(differenceInMilli))
		.append(" -hours:")
		.append(TimeUnit.MILLISECONDS.toHours(differenceInMilli));
		
		SNSHandler.sendSimpleMessage(sbMsg.toString(), "@ViafouraImport! Good");
	}
	
	private void callViafouraServices(DateRange dateRange) {	
		
		String parameters = "?from=fromDateTimestamp&to=toDateTimestamp";
		parameters = parameters.replace("fromDateTimestamp", dateRange.getFromDateTimestamp());
		parameters = parameters.replace("toDateTimestamp", dateRange.getToDateTimestamp());
		
		try{
			Timestamp fromStamp = new Timestamp(Long.valueOf(dateRange.getFromDateTimestamp()));
			Timestamp toStamp = new Timestamp(Long.valueOf(dateRange.getToDateTimestamp()));
			
			LoggingUtils.logInfo("******************  From Date = " + new Date(fromStamp.getTime()) );
			LoggingUtils.logInfo("******************  To Date = " + new Date(toStamp.getTime()) );			
			
			StringBuilder failedURLs = new StringBuilder("Viafoura service call failed even after calling three times in row, the process aborted ! \n ");					
			for(APIURIEnum aPIURIEnum : APIURIEnum.values()){	
				try{					
					if(PropertyValue.USE_OVERALL_GATEHOUSE_AGGREGATE_SCOPE == null || "no".equals(PropertyValue.USE_OVERALL_GATEHOUSE_AGGREGATE_SCOPE)){
						int apiUrlFailedCount = 0;
						for(DomainScopeEnum domainScope : DomainScopeEnum.values()){
							StringBuilder exceptioMessage = new StringBuilder();
							
							String serviceURL = baseURL + domainScope.getScope() + aPIURIEnum.getUrl() + parameters;		
							OAuth2RestTemplate oauth2RestTemplate = getRestTemplate(domainScope.getScope());
							
							String responseJson = null;
							int serviceFailedCount = 0;
							do{
								try{
									if(serviceFailedCount == 3){
										if(apiUrlFailedCount == 3){
											failedURLs.append(serviceURL)
													.append("  \n ")
													.append(exceptioMessage)
													.append(" \n ");
										}
										break;
									}
									
									responseJson = oauth2RestTemplate.getForObject(serviceURL, String.class);
								}catch(Exception e){
									apiUrlFailedCount++;
									LoggingUtils.logInfo("Exception while calling using individual scope ! serviceURL : " + serviceURL);
									
									String exceptionString = LoggingUtils.getExceptionString(e);
									if(exceptionString != null && exceptionString.length() > 90 && apiUrlFailedCount == 1){
										exceptioMessage.append(exceptionString.substring(0, 86));
									}
									serviceFailedCount++;
									
									Thread.sleep(2000); // 2 seconds 
								}
							}while(serviceFailedCount > 0);
							
							if(responseJson == null){
								continue;
							}else{
								StringBuilder scopeSB = new StringBuilder();
								scopeSB.append("{ \n \"scope\" : ").append("\"").append(domainScope.getScope()).append("\", ");
								responseJson = responseJson.replaceFirst(Pattern.quote("{"), scopeSB.toString());
							}
							
							mapBean(aPIURIEnum.getUrl(), responseJson);
							
					       Thread.sleep(50);
						}
						if(apiUrlFailedCount > 3){
							failedURLs.append(" Because of no of Domains/Scopes the above service url failed this no of time : ")
							.append(apiUrlFailedCount)
							.append(" \n ");
						}
					}else{
						String serviceURL = baseURL + ApplicationConstants.OVERALL_GATEHOUSE_AGGREGATE_SCOPE + aPIURIEnum.getUrl() + parameters;		
						OAuth2RestTemplate oauth2RestTemplate = getRestTemplate(ApplicationConstants.OVERALL_GATEHOUSE_AGGREGATE_SCOPE);
						
						LoggingUtils.logInfo(serviceURL);
						
						String responseJson = oauth2RestTemplate.getForObject(serviceURL, String.class);
						
						if(responseJson == null){
							//continue;
						}else{
							StringBuilder scopeSB = new StringBuilder();
							scopeSB.append("{ \n \"scope\" : ").append("\"").append(ApplicationConstants.OVERALL_GATEHOUSE_AGGREGATE_SCOPE).append("\", ");
							responseJson = responseJson.replaceFirst(Pattern.quote("{"), scopeSB.toString());
						}
						
					   mapBean(aPIURIEnum.getUrl(), responseJson);	
					}				
				}catch(Exception e){
					//discard exception
					e.printStackTrace();
					
				}
			}
			if(failedURLs.length() > 110){
				SNSHandler.sendSimpleMessage(failedURLs.toString(), "@ViafouraImport! FailedURLs");
			}
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}	
	}
	
	private OAuth2RestTemplate getRestTemplate(String scope) {
		ClientCredentialsResourceDetails resourceDetails = null;
		try{
			resourceDetails = new ClientCredentialsResourceDetails();
			resourceDetails.setGrantType("client_credentials");
			resourceDetails.setAccessTokenUri("https://auth.viafoura.io/authorize_client");
		
			//-- set the clients info
			resourceDetails.setClientId(PropertyValue.CLIENT_ID);
			resourceDetails.setClientSecret(PropertyValue.CLIENT_SECRET);
			
			// set scopes
			List<String> scopes = new ArrayList<>();
			scopes.add(scope); 
			resourceDetails.setScope(scopes);
			
			return new OAuth2RestTemplate(resourceDetails);				
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}
		
		return null;
	}
	
	private void mapBean(String url, String json) throws Exception{
		
		if(url.equals(APIURIEnum.E_VISITORS_ANONYMOUS_TOTAL.getUrl())){
			persistenceService.saveEAnonymousTotal(json);	
			
		} else if(url.equals(APIURIEnum.E_NOTIFICATIONS_BROWSER_CLICKED.getUrl())){
			persistenceService.saveEBrowserClicked(json);
			
		}else if(url.equals(APIURIEnum.E_NOTIFICATIONS_BROWSER_RECEIVED.getUrl())){
			persistenceService.saveEBrowserReceived(json);	
			
		}else if(url.equals(APIURIEnum.E_INDICATOR_CLICKS_TOTAL.getUrl())){
			persistenceService.saveEClicksTotal(json);		
			
		} else if(url.equals(APIURIEnum.E_LOADS_JS_TOTAL.getUrl())){
			persistenceService.saveEJsTotal(json);
			
		}else  if(url.equals(APIURIEnum.E_TRAY_OPENS_TOTAL.getUrl())){
			persistenceService.saveEOpensTotal(json);	
			
		}else if(url.equals(APIURIEnum.E_VISITORS_ATTENTION.getUrl())){
			persistenceService.saveEVisitorsAttention(json);	
			
		}else if(url.equals(APIURIEnum.E_VISITORS_USER_AGENTS.getUrl())){
			persistenceService.saveEVisitorsUserAgents(json);	
			
		}else if(url.equals(APIURIEnum.E_VISITORS_VISITS.getUrl())){
			persistenceService.saveEVisitorsVisits(json);	
			
		}else if(url.equals(APIURIEnum.U_ANONYMOUS_TOTAL.getUrl())){
			persistenceService.saveUAnonymousTotal(json);	
			
		}else if(url.equals(APIURIEnum.U_COMMENTS.getUrl())){
			persistenceService.saveUComments(json);	
			
		}else if(url.equals(APIURIEnum.U_DISLIKES.getUrl())){
			persistenceService.saveUDislikes(json);	
			
		}else if(url.equals(APIURIEnum.U_FOLLOWS_PAGES.getUrl())){
			persistenceService.saveUFollowsPages(json);;	
			
		}else if(url.equals(APIURIEnum.U_FOLLOWS_USERS.getUrl())){
			persistenceService.saveUFollowsUsers(json);	
			
		}else if(url.equals(APIURIEnum.U_LIKES.getUrl())){
			persistenceService.saveULikes(json);
			
		}else if(url.equals(APIURIEnum.U_LOGINS.getUrl())){
			persistenceService.saveULogins(json);	
			
		}else if(url.equals(APIURIEnum.U_RATINGS.getUrl())){
			persistenceService.saveURatings(json);	
			
		}else if(url.equals(APIURIEnum.U_REGISTERED.getUrl())){
			persistenceService.saveURegistered(json);	
			
		}else if(url.equals(APIURIEnum.U_REGISTERED_TOTAL.getUrl())){
			persistenceService.saveURegisteredTotal(json);	
			
		}else if(url.equals(APIURIEnum.U_SCHEDULE.getUrl())){
			persistenceService.saveUSchedule(json);
			
		}else if(url.equals(APIURIEnum.U_TOPICS_SUBSCRIBED.getUrl())){
			persistenceService.saveUTopicsSubscribed(json);
			
		}else if(url.equals(APIURIEnum.U_TOPICS_UNSUBSCRIBED.getUrl())){
			persistenceService.saveUTopicsUnsubscribed(json);	
			
		}else if(url.equals(APIURIEnum.U_VISITS.getUrl())){
			persistenceService.saveUVisits(json);	
			
		}
	}
}
