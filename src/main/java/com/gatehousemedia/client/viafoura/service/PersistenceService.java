package com.gatehousemedia.client.viafoura.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.gatehousemedia.client.viafoura.db.domain.EAnonymousTotal;
import com.gatehousemedia.client.viafoura.db.domain.EBrowserClicked;
import com.gatehousemedia.client.viafoura.db.domain.EBrowserReceived;
import com.gatehousemedia.client.viafoura.db.domain.EClicksTotal;
import com.gatehousemedia.client.viafoura.db.domain.EJsTotal;
import com.gatehousemedia.client.viafoura.db.domain.EOpensTotal;
import com.gatehousemedia.client.viafoura.db.domain.EVisitorsAttention;
import com.gatehousemedia.client.viafoura.db.domain.EVisitorsUserAgents;
import com.gatehousemedia.client.viafoura.db.domain.EVisitorsVisits;
import com.gatehousemedia.client.viafoura.db.domain.UAnonymousTotal;
import com.gatehousemedia.client.viafoura.db.domain.UComments;
import com.gatehousemedia.client.viafoura.db.domain.UDislikes;
import com.gatehousemedia.client.viafoura.db.domain.UFollowsPages;
import com.gatehousemedia.client.viafoura.db.domain.UFollowsUsers;
import com.gatehousemedia.client.viafoura.db.domain.ULikes;
import com.gatehousemedia.client.viafoura.db.domain.ULogins;
import com.gatehousemedia.client.viafoura.db.domain.URatings;
import com.gatehousemedia.client.viafoura.db.domain.URegistered;
import com.gatehousemedia.client.viafoura.db.domain.URegisteredTotal;
import com.gatehousemedia.client.viafoura.db.domain.USchedule;
import com.gatehousemedia.client.viafoura.db.domain.UTopicsSubscribed;
import com.gatehousemedia.client.viafoura.db.domain.UTopicsUnsubscribed;
import com.gatehousemedia.client.viafoura.db.domain.UVisits;
import com.gatehousemedia.client.viafoura.db.repo.EAnonymousTotalRepo;
import com.gatehousemedia.client.viafoura.db.repo.EBrowserClickedRepo;
import com.gatehousemedia.client.viafoura.db.repo.EBrowserReceivedRepo;
import com.gatehousemedia.client.viafoura.db.repo.EClicksTotalRepo;
import com.gatehousemedia.client.viafoura.db.repo.EJsTotalRepo;
import com.gatehousemedia.client.viafoura.db.repo.EOpensTotalRepo;
import com.gatehousemedia.client.viafoura.db.repo.EVisitorsAttentionRepo;
import com.gatehousemedia.client.viafoura.db.repo.EVisitorsUserAgentsRepo;
import com.gatehousemedia.client.viafoura.db.repo.EVisitorsVisitsRepo;
import com.gatehousemedia.client.viafoura.db.repo.UAnonymousTotalRepo;
import com.gatehousemedia.client.viafoura.db.repo.UCommentsRepo;
import com.gatehousemedia.client.viafoura.db.repo.UDislikesRepo;
import com.gatehousemedia.client.viafoura.db.repo.UFollowsPagesRepo;
import com.gatehousemedia.client.viafoura.db.repo.UFollowsUsersRepo;
import com.gatehousemedia.client.viafoura.db.repo.ULikesRepo;
import com.gatehousemedia.client.viafoura.db.repo.ULoginsRepo;
import com.gatehousemedia.client.viafoura.db.repo.URatingsRepo;
import com.gatehousemedia.client.viafoura.db.repo.URegisteredRepo;
import com.gatehousemedia.client.viafoura.db.repo.URegisteredTotalRepo;
import com.gatehousemedia.client.viafoura.db.repo.UScheduleRepo;
import com.gatehousemedia.client.viafoura.db.repo.UTopicsSubscribedRepo;
import com.gatehousemedia.client.viafoura.db.repo.UTopicsUnsubscribedRepo;
import com.gatehousemedia.client.viafoura.db.repo.UVisitsRepo;
import com.gatehousemedia.client.viafoura.json.bean.AddressableUser;
import com.gatehousemedia.client.viafoura.json.bean.EAnonymousTotalBean;
import com.gatehousemedia.client.viafoura.json.bean.EBrowserClickedBean;
import com.gatehousemedia.client.viafoura.json.bean.EBrowserReceivedBean;
import com.gatehousemedia.client.viafoura.json.bean.EClicksTotalBean;
import com.gatehousemedia.client.viafoura.json.bean.EJSTotalBean;
import com.gatehousemedia.client.viafoura.json.bean.EOpensTotalBean;
import com.gatehousemedia.client.viafoura.json.bean.EVisitorsAttentionBean;
import com.gatehousemedia.client.viafoura.json.bean.EVisitorsUserAgentsBean;
import com.gatehousemedia.client.viafoura.json.bean.EVisitorsVisitsBean;
import com.gatehousemedia.client.viafoura.json.bean.UAnonymousTotalBean;
import com.gatehousemedia.client.viafoura.json.bean.UCommentsBean;
import com.gatehousemedia.client.viafoura.json.bean.UFollowsPagesBean;
import com.gatehousemedia.client.viafoura.json.bean.UFollowsUsersBean;
import com.gatehousemedia.client.viafoura.json.bean.ULoginsBean;
import com.gatehousemedia.client.viafoura.json.bean.URegisteredBean;
import com.gatehousemedia.client.viafoura.json.bean.URegisteredTotalBean;
import com.gatehousemedia.client.viafoura.json.bean.UScheduleBean;
import com.gatehousemedia.client.viafoura.json.bean.UTopicsSubscribedBean;
import com.gatehousemedia.client.viafoura.json.bean.UTopicsUnsubscribedBean;
import com.gatehousemedia.client.viafoura.json.bean.UVisitsBean;
import com.gatehousemedia.client.viafoura.json.bean.UserAggregatePair;
import com.gatehousemedia.client.viafoura.json.bean.UserNotificationAggregates;
import com.gatehousemedia.client.viafoura.json.bean.UserTimestampsPair;
import com.gatehousemedia.client.viafoura.json.bean.UserTopicsPair;
import com.gatehousemedia.client.viafoura.json.bean.UserUserAgentsPair;
import com.gatehousemedia.client.viafoura.json.bean.UserVisitsPair;
import com.gatehousemedia.client.viafoura.util.LoggingUtils;

@Component(value="persistenceService")
public class PersistenceService {
	
	@Autowired
	protected EClicksTotalRepo eClicksTotalRepo;
	
	@Autowired
	protected EAnonymousTotalRepo eAnonymousTotalRepo;
	
	@Autowired
	protected EBrowserClickedRepo eBrowserClickedRepo;
	
	@Autowired
	protected EBrowserReceivedRepo eBrowserReceivedRepo;
	
	@Autowired
	protected EJsTotalRepo eJsTotalRepo;
	
	@Autowired
	protected EOpensTotalRepo eOpensTotalRepo;
	
	@Autowired
	protected  EVisitorsAttentionRepo eVisitorsAttentionRepo;
	
	@Autowired
	protected EVisitorsUserAgentsRepo eVisitorsUserAgentsRepo;
	
	@Autowired
	protected EVisitorsVisitsRepo eVisitorsVisitsRepo;
	
	@Autowired
	protected UAnonymousTotalRepo uAnonymousTotalRepo;
	
	@Autowired
	protected UCommentsRepo uCommentsRepo;
	
	@Autowired
	protected UDislikesRepo uDislikesRepo;
	
	@Autowired
	protected UFollowsPagesRepo uFollowsPagesRepo;
	
	@Autowired
	protected UFollowsUsersRepo uFollowsUsersRepo;
	
	@Autowired
	protected ULikesRepo uLikesRepo;	
	
	@Autowired
	protected ULoginsRepo uLoginsRepo;	
	
	@Autowired
	protected URatingsRepo uRatingsRepo;	
	
	@Autowired
	protected URegisteredRepo uRegisteredRepo;	
	
	@Autowired
	protected URegisteredTotalRepo uRegisteredTotalRepo;		
	
	@Autowired
	protected UScheduleRepo uScheduleRepo;	
	
	@Autowired
	protected UTopicsSubscribedRepo uTopicsSubscribedRepo;
	
	@Autowired
	protected UTopicsUnsubscribedRepo uTopicsUnsubscribedRepo;
	
	@Autowired
	protected UVisitsRepo uVisitsRepo;
	
	public void saveEAnonymousTotal(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			EAnonymousTotalBean eAnonymousTotalBean = mapper.readValue(json, EAnonymousTotalBean.class);
		
			if(eAnonymousTotalBean != null){
				EAnonymousTotal eAnonymousTotal = new EAnonymousTotal();
				eAnonymousTotal.setAggregate(BigInteger.valueOf(eAnonymousTotalBean.getAggregate()));
				eAnonymousTotal.setScope(eAnonymousTotalBean.getScope());
				
				eAnonymousTotalRepo.save(eAnonymousTotal);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}
	}
		
	public void saveEBrowserClicked(String json){

		try{
			ObjectMapper mapper = new ObjectMapper();
			EBrowserClickedBean eBrowserClickedBean = mapper.readValue(json, EBrowserClickedBean.class);
		
			if(eBrowserClickedBean != null){				
				
				List<EBrowserClicked> eBrowserClickeds = new ArrayList<EBrowserClicked>();
				
				if(eBrowserClickedBean.getAnonymous() != null && !eBrowserClickedBean.getAnonymous().isEmpty()){
					
					for(UserNotificationAggregates userNotificationAggregates : eBrowserClickedBean.getAnonymous()){
						EBrowserClicked eBrowserClicked = new EBrowserClicked();
						eBrowserClicked.setBroadcast(BigInteger.valueOf(userNotificationAggregates.getBroadcast()));
						eBrowserClicked.setIsRegistered(Boolean.FALSE);
						eBrowserClicked.setReply(BigInteger.valueOf(userNotificationAggregates.getReply()));
						eBrowserClicked.setScope(eBrowserClickedBean.getScope());
						eBrowserClicked.setTopic(BigInteger.valueOf(userNotificationAggregates.getTopic()));
						eBrowserClicked.setUuid(userNotificationAggregates.getUuid());
						
						eBrowserClickeds.add(eBrowserClicked);
					}
				}else{
					
					for(UserNotificationAggregates userNotificationAggregates : eBrowserClickedBean.getRegistered()){
						EBrowserClicked eBrowserClicked = new EBrowserClicked();
						eBrowserClicked.setBroadcast(BigInteger.valueOf(userNotificationAggregates.getBroadcast()));
						eBrowserClicked.setIsRegistered(Boolean.TRUE);
						eBrowserClicked.setReply(BigInteger.valueOf(userNotificationAggregates.getReply()));
						eBrowserClicked.setScope(eBrowserClickedBean.getScope());
						eBrowserClicked.setTopic(BigInteger.valueOf(userNotificationAggregates.getTopic()));
						eBrowserClicked.setUuid(userNotificationAggregates.getUuid());
						
						eBrowserClickeds.add(eBrowserClicked);
					}
				}
				if(!eBrowserClickeds.isEmpty()){
					eBrowserClickedRepo.save(eBrowserClickeds);
				}
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}	
	}
	
	public void saveEBrowserReceived(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			EBrowserReceivedBean eBrowserReceivedBean = mapper.readValue(json, EBrowserReceivedBean.class);
		
			if(eBrowserReceivedBean != null){
				List<EBrowserReceived> eBrowserReceiveds = new ArrayList<EBrowserReceived>();
				
				if(eBrowserReceivedBean.getAnonymous() != null && !eBrowserReceivedBean.getAnonymous().isEmpty()){
					
					for(UserNotificationAggregates userNotificationAggregates : eBrowserReceivedBean.getAnonymous()){
						EBrowserReceived eBrowserReceived = new EBrowserReceived();
						eBrowserReceived.setBroadcast(BigInteger.valueOf(userNotificationAggregates.getBroadcast()));
						eBrowserReceived.setIsRegistered(Boolean.FALSE);
						eBrowserReceived.setReply(BigInteger.valueOf(userNotificationAggregates.getReply()));
						eBrowserReceived.setScope(eBrowserReceivedBean.getScope());
						eBrowserReceived.setTopic(BigInteger.valueOf(userNotificationAggregates.getTopic()));
						eBrowserReceived.setUuid(userNotificationAggregates.getUuid());
						
						eBrowserReceiveds.add(eBrowserReceived);
					}	
				}else{					
					for(UserNotificationAggregates userNotificationAggregates : eBrowserReceivedBean.getRegistered()){
						EBrowserReceived eBrowserReceived = new EBrowserReceived();
						eBrowserReceived.setBroadcast(BigInteger.valueOf(userNotificationAggregates.getBroadcast()));
						eBrowserReceived.setIsRegistered(Boolean.TRUE);
						eBrowserReceived.setReply(BigInteger.valueOf(userNotificationAggregates.getReply()));
						eBrowserReceived.setScope(eBrowserReceivedBean.getScope());
						eBrowserReceived.setTopic(BigInteger.valueOf(userNotificationAggregates.getTopic()));
						eBrowserReceived.setUuid(userNotificationAggregates.getUuid());
						
						eBrowserReceiveds.add(eBrowserReceived);
					}					
				}				
				if(!eBrowserReceiveds.isEmpty()){
					eBrowserReceivedRepo.save(eBrowserReceiveds);
				}		
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}	
	}
	
	public void saveEClicksTotal(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			EClicksTotalBean eClicksTotalBean = mapper.readValue(json, EClicksTotalBean.class);

			List<EClicksTotal> eClicksTotalHttp = new ArrayList<EClicksTotal>();
			for(UserAggregatePair userAggregatePair : eClicksTotalBean.getResult()){
				EClicksTotal eClicksTotal = new EClicksTotal();
				eClicksTotal.setScope(eClicksTotalBean.getScope());
				eClicksTotal.setAggregate(BigInteger.valueOf(userAggregatePair.getAggregate()));
				eClicksTotal.setUser(userAggregatePair.getUser());
				
				eClicksTotalHttp.add(eClicksTotal);
			}
			
			if(!eClicksTotalHttp.isEmpty()){
				eClicksTotalRepo.save(eClicksTotalHttp);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}		
	}

	public void saveEJsTotal(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			EJSTotalBean eJSTotalBean = mapper.readValue(json, EJSTotalBean.class);
		
			if(eJSTotalBean != null){
				EJsTotal eJsTotal = new EJsTotal();
				eJsTotal.setAggregate(BigInteger.valueOf(eJSTotalBean.getAggregate()));
				eJsTotal.setScope(eJSTotalBean.getScope());
				
				eJsTotalRepo.save(eJsTotal);
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}
	}
	
	public void saveEOpensTotal(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			EOpensTotalBean eOpensTotalBean = mapper.readValue(json, EOpensTotalBean.class);

			List<EOpensTotal> eOpensTotals = new ArrayList<EOpensTotal>();
			for(UserAggregatePair userAggregatePair : eOpensTotalBean.getResult()){
				EOpensTotal eEOpensTotal = new EOpensTotal();
				eEOpensTotal.setScope(eOpensTotalBean.getScope());
				eEOpensTotal.setAggregate(BigInteger.valueOf(userAggregatePair.getAggregate()));
				eEOpensTotal.setUser(userAggregatePair.getUser());
				
				eOpensTotals.add(eEOpensTotal);
			}
			
			if(!eOpensTotals.isEmpty()){
				eOpensTotalRepo.save(eOpensTotals);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}		
	}
	
	public void saveEVisitorsAttention(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			EVisitorsAttentionBean eVisitorsAttentionBean = mapper.readValue(json, EVisitorsAttentionBean.class);

			List<EVisitorsAttention> eVisitorsAttentions = new ArrayList<EVisitorsAttention>();
			for(UserAggregatePair userAggregatePair : eVisitorsAttentionBean.getResult()){
				EVisitorsAttention eVisitorsAttention = new EVisitorsAttention();
				eVisitorsAttention.setScope(eVisitorsAttentionBean.getScope());
				eVisitorsAttention.setAggregate(BigInteger.valueOf(userAggregatePair.getAggregate()));
				eVisitorsAttention.setUser(userAggregatePair.getUser());
				
				eVisitorsAttentions.add(eVisitorsAttention);
			}
			
			if(!eVisitorsAttentions.isEmpty()){
				eVisitorsAttentionRepo.save(eVisitorsAttentions);
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}		
	}
	
	public void saveEVisitorsUserAgents(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			
			EVisitorsUserAgentsBean eVisitorsUserAgentsBean = mapper.readValue(json, EVisitorsUserAgentsBean.class);

			List<EVisitorsUserAgents> eVisitorsUserAgentses = new ArrayList<EVisitorsUserAgents>();
			for(UserUserAgentsPair userUserAgentsPair : eVisitorsUserAgentsBean.getResult()){
				EVisitorsUserAgents eVisitorsUserAgents = new EVisitorsUserAgents();
				eVisitorsUserAgents.setScope(eVisitorsUserAgentsBean.getScope());
				eVisitorsUserAgents.setUser(userUserAgentsPair.getUser());
				
				String arrayToJson = mapper.writeValueAsString(userUserAgentsPair.getUser_agents());
				if(arrayToJson != null){
					eVisitorsUserAgents.setUserAgents(arrayToJson.getBytes());
				}
				
				eVisitorsUserAgentses.add(eVisitorsUserAgents);
			}
			
			if(!eVisitorsUserAgentses.isEmpty()){
				eVisitorsUserAgentsRepo.save(eVisitorsUserAgentses);
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}		
	}
	
	public void saveEVisitorsVisits(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			
			EVisitorsVisitsBean eVisitorsVisitsBean = mapper.readValue(json, EVisitorsVisitsBean.class);

			List<EVisitorsVisits> eVisitorsVisitses = new ArrayList<EVisitorsVisits>();
			for(UserVisitsPair userVisitsPair : eVisitorsVisitsBean.getResult()){
				EVisitorsVisits eVisitorsVisits = new EVisitorsVisits();
				eVisitorsVisits.setScope(eVisitorsVisitsBean.getScope());
				eVisitorsVisits.setUuid(userVisitsPair.getUuid());
				
				String arrayToJson = mapper.writeValueAsString(userVisitsPair.getVisits());
				if(arrayToJson != null){
					eVisitorsVisits.setVisits(arrayToJson.getBytes());
				}
				
				eVisitorsVisitses.add(eVisitorsVisits);
			}
			
			if(!eVisitorsVisitses.isEmpty()){
				eVisitorsVisitsRepo.save(eVisitorsVisitses);
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}		
	}
	
	public void saveUAnonymousTotal(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			UAnonymousTotalBean uAnonymousTotalBean = mapper.readValue(json, UAnonymousTotalBean.class);
		
			if(uAnonymousTotalBean != null){
				UAnonymousTotal uAnonymousTotal = new UAnonymousTotal();
				uAnonymousTotal.setAggregate(BigInteger.valueOf(uAnonymousTotalBean.getAggregate()));
				uAnonymousTotal.setScope(uAnonymousTotalBean.getScope());
				
				uAnonymousTotalRepo.save(uAnonymousTotal);
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}
	}
	
	public void saveUComments(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			UCommentsBean uCommentsBean = mapper.readValue(json, UCommentsBean.class);

			List<UComments> uCommentses = new ArrayList<UComments>();
			for(UserAggregatePair userAggregatePair : uCommentsBean.getResult()){
				UComments uComments = new UComments();
				uComments.setScope(uCommentsBean.getScope());
				uComments.setAggregate(BigInteger.valueOf(userAggregatePair.getAggregate()));
				uComments.setUser(userAggregatePair.getUser());
				
				uCommentses.add(uComments);
			}
			
			if(!uCommentses.isEmpty()){
				uCommentsRepo.save(uCommentses);
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}		
	}
	
	public void saveUDislikes(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			UCommentsBean uCommentsBean = mapper.readValue(json, UCommentsBean.class);

			List<UDislikes> uDislikeses = new ArrayList<UDislikes>();
			for(UserAggregatePair userAggregatePair : uCommentsBean.getResult()){
				UDislikes uDislikes = new UDislikes();
				uDislikes.setScope(uCommentsBean.getScope());
				uDislikes.setAggregate(BigInteger.valueOf(userAggregatePair.getAggregate()));
				uDislikes.setUser(userAggregatePair.getUser());
				
				uDislikeses.add(uDislikes);
			}
			
			if(!uDislikeses.isEmpty()){
				uDislikesRepo.save(uDislikeses);
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}		
	}	
	
	public void saveUFollowsPages(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			UFollowsPagesBean uFollowsPagesBean = mapper.readValue(json, UFollowsPagesBean.class);

			List<UFollowsPages> uFollowsPageses = new ArrayList<UFollowsPages>();
			for(UserAggregatePair userAggregatePair : uFollowsPagesBean.getResult()){
				UFollowsPages uFollowsPages = new UFollowsPages();
				uFollowsPages.setScope(uFollowsPagesBean.getScope());
				uFollowsPages.setAggregate(BigInteger.valueOf(userAggregatePair.getAggregate()));
				uFollowsPages.setUser(userAggregatePair.getUser());
				
				uFollowsPageses.add(uFollowsPages);
			}
			
			if(!uFollowsPageses.isEmpty()){
				uFollowsPagesRepo.save(uFollowsPageses);
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}		
	}
	
	public void saveUFollowsUsers(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			UFollowsUsersBean uFollowsUsersBean = mapper.readValue(json, UFollowsUsersBean.class);

			List<UFollowsUsers> uFollowsUserses = new ArrayList<UFollowsUsers>();
			for(UserAggregatePair userAggregatePair : uFollowsUsersBean.getResult()){
				UFollowsUsers uFollowsUsers = new UFollowsUsers();
				uFollowsUsers.setScope(uFollowsUsersBean.getScope());
				uFollowsUsers.setAggregate(BigInteger.valueOf(userAggregatePair.getAggregate()));
				uFollowsUsers.setUser(userAggregatePair.getUser());
				
				uFollowsUserses.add(uFollowsUsers);
			}
			
			if(!uFollowsUserses.isEmpty()){
				uFollowsUsersRepo.save(uFollowsUserses);
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}		
	}
	
	public void saveULikes(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			UFollowsUsersBean uFollowsUsersBean = mapper.readValue(json, UFollowsUsersBean.class);

			List<ULikes> uLikeses = new ArrayList<ULikes>();
			for(UserAggregatePair userAggregatePair : uFollowsUsersBean.getResult()){
				ULikes uLikes = new ULikes();
				uLikes.setScope(uFollowsUsersBean.getScope());
				uLikes.setAggregate(BigInteger.valueOf(userAggregatePair.getAggregate()));
				uLikes.setUser(userAggregatePair.getUser());
				
				uLikeses.add(uLikes);
			}
			
			if(!uLikeses.isEmpty()){
				uLikesRepo.save(uLikeses);
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}		
	}
	
	public void saveULogins(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			ULoginsBean uLoginsBean = mapper.readValue(json, ULoginsBean.class);

			List<ULogins> uLoginses = new ArrayList<ULogins>();
			for(UserAggregatePair userAggregatePair : uLoginsBean.getResult()){
				ULogins uLogins = new ULogins();
				uLogins.setScope(uLoginsBean.getScope());
				uLogins.setAggregate(BigInteger.valueOf(userAggregatePair.getAggregate()));
				uLogins.setUser(userAggregatePair.getUser());
				
				uLoginses.add(uLogins);
			}
			
			if(!uLoginses.isEmpty()){
				uLoginsRepo.save(uLoginses);
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}		
	}	
	
	public void saveURatings(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			ULoginsBean uLoginsBean = mapper.readValue(json, ULoginsBean.class);

			List<URatings> uRatingses = new ArrayList<URatings>();
			for(UserAggregatePair userAggregatePair : uLoginsBean.getResult()){
				URatings uRatings = new URatings();
				uRatings.setScope(uLoginsBean.getScope());
				uRatings.setAggregate(BigInteger.valueOf(userAggregatePair.getAggregate()));
				uRatings.setUser(userAggregatePair.getUser());
				
				uRatingses.add(uRatings);
			}
			
			if(!uRatingses.isEmpty()){
				uRatingsRepo.save(uRatingses);
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}		
	}
	
	public void saveURegistered(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			
			URegisteredBean uRegisteredBean = mapper.readValue(json, URegisteredBean.class);
			

			List<URegistered> uRegistereds = new ArrayList<URegistered>();
			for(AddressableUser addressableUser : uRegisteredBean.getResult()){
				URegistered uRegistered = new URegistered();
				uRegistered.setEmail(addressableUser.getEmail());
				uRegistered.setName(addressableUser.getName());
				uRegistered.setScope(uRegisteredBean.getScope());
				uRegistered.setUuid(addressableUser.getUuid());
				
				String arrayToJson = mapper.writeValueAsString(addressableUser.getSocial());
				if(arrayToJson != null){
					uRegistered.setSocial(arrayToJson.getBytes());
				}
				
				uRegistereds.add(uRegistered);
			}
			
			if(!uRegistereds.isEmpty()){
				uRegisteredRepo.save(uRegistereds);
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}		
	}	
	
	public void saveURegisteredTotal(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			URegisteredTotalBean uRegisteredTotalBean = mapper.readValue(json, URegisteredTotalBean.class);
		
			if(uRegisteredTotalBean != null){
				URegisteredTotal uRegisteredTotal = new URegisteredTotal();
				uRegisteredTotal.setAggregate(BigInteger.valueOf(uRegisteredTotalBean.getAggregate()));
				uRegisteredTotal.setScope(uRegisteredTotalBean.getScope());
				
				uRegisteredTotalRepo.save(uRegisteredTotal);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}
	}
		
	public void saveUSchedule(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			
			UScheduleBean uScheduleBean = mapper.readValue(json, UScheduleBean.class);
			
			List<USchedule> uSchedules = new ArrayList<USchedule>();
			for(UserTimestampsPair userTimestampsPair : uScheduleBean.getResult()){
				USchedule uSchedule = new USchedule();
				uSchedule.setScope(uScheduleBean.getScope());
				uSchedule.setUser(userTimestampsPair.getUser());
				
				String arrayToJson = mapper.writeValueAsString(userTimestampsPair.getView_timestamps());
				if(arrayToJson != null){
					uSchedule.setViewTimestamps(arrayToJson.getBytes());
				}
				
				uSchedules.add(uSchedule);
			}
			
			if(!uSchedules.isEmpty()){
				uScheduleRepo.save(uSchedules);
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}		
	}
	
	public void saveUTopicsSubscribed(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			
			UTopicsSubscribedBean uTopicsSubscribedBean = mapper.readValue(json, UTopicsSubscribedBean.class);
			
			List<UTopicsSubscribed> UTopicsSubscribeds = new ArrayList<UTopicsSubscribed>();
			
			for(UserTopicsPair userTopicsPair : uTopicsSubscribedBean.getResult()){
				UTopicsSubscribed uTopicsSubscribed = new UTopicsSubscribed();
				uTopicsSubscribed.setScope(uTopicsSubscribedBean.getScope());
				uTopicsSubscribed.setUser(userTopicsPair.getUser());
				
				String arrayToJson = mapper.writeValueAsString(userTopicsPair.getTopics());
				if(arrayToJson != null){
					uTopicsSubscribed.setTopics(arrayToJson.getBytes());
				}
				
				UTopicsSubscribeds.add(uTopicsSubscribed);
			}
			
			if(!UTopicsSubscribeds.isEmpty()){
				uTopicsSubscribedRepo.save(UTopicsSubscribeds);
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}		
	}
	
	public void saveUTopicsUnsubscribed(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			
			UTopicsUnsubscribedBean uTopicsUnsubscribedBean = mapper.readValue(json, UTopicsUnsubscribedBean.class);
			
			List<UTopicsUnsubscribed> uTopicsUnsubscribeds = new ArrayList<UTopicsUnsubscribed>();
			
			for(UserTopicsPair userTopicsPair : uTopicsUnsubscribedBean.getResult()){
				UTopicsUnsubscribed uTopicsUnsubscribed = new UTopicsUnsubscribed();
				uTopicsUnsubscribed.setScope(uTopicsUnsubscribedBean.getScope());
				uTopicsUnsubscribed.setUser(userTopicsPair.getUser());
				
				String arrayToJson = mapper.writeValueAsString(userTopicsPair.getTopics());
				if(arrayToJson != null){
					uTopicsUnsubscribed.setTopics(arrayToJson.getBytes());
				}
				
				uTopicsUnsubscribeds.add(uTopicsUnsubscribed);
			}
			
			if(!uTopicsUnsubscribeds.isEmpty()){
				uTopicsUnsubscribedRepo.save(uTopicsUnsubscribeds);
			}			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}		
	}
	
	public void saveUVisits(String json){
		try{
			ObjectMapper mapper = new ObjectMapper();
			UVisitsBean uVisitsBean = mapper.readValue(json, UVisitsBean.class);

			List<UVisits> uVisitses = new ArrayList<UVisits>();
			for(UserAggregatePair userAggregatePair : uVisitsBean.getResult()){
				UVisits uVisits = new UVisits();
				uVisits.setScope(uVisitsBean.getScope());
				uVisits.setAggregate(BigInteger.valueOf(userAggregatePair.getAggregate()));
				uVisits.setUser(userAggregatePair.getUser());
				
				uVisitses.add(uVisits);
			}
			
			if(!uVisitses.isEmpty()){
				uVisitsRepo.save(uVisitses);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}		
	}
	
}
