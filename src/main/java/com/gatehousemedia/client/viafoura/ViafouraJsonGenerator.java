package com.gatehousemedia.client.viafoura;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.gatehousemedia.client.viafoura.constant.PropertyValue;
import com.gatehousemedia.client.viafoura.db.domain.EAnonymousTotal;
import com.gatehousemedia.client.viafoura.db.domain.EBrowserClicked;
import com.gatehousemedia.client.viafoura.db.domain.EBrowserReceived;
import com.gatehousemedia.client.viafoura.db.domain.EClicksTotal;
import com.gatehousemedia.client.viafoura.db.domain.EJsTotal;
import com.gatehousemedia.client.viafoura.db.domain.EOpensTotal;
import com.gatehousemedia.client.viafoura.db.domain.EVisitorsAttention;
import com.gatehousemedia.client.viafoura.db.domain.EVisitorsUserAgents;
import com.gatehousemedia.client.viafoura.db.domain.EVisitorsVisits;
import com.gatehousemedia.client.viafoura.db.domain.RegisteredDistinct;
import com.gatehousemedia.client.viafoura.db.domain.UAnonymousTotal;
import com.gatehousemedia.client.viafoura.db.domain.UComments;
import com.gatehousemedia.client.viafoura.db.domain.UDislikes;
import com.gatehousemedia.client.viafoura.db.domain.UFollowsPages;
import com.gatehousemedia.client.viafoura.db.domain.UFollowsUsers;
import com.gatehousemedia.client.viafoura.db.domain.ULikes;
import com.gatehousemedia.client.viafoura.db.domain.ULogins;
import com.gatehousemedia.client.viafoura.db.domain.URatings;
import com.gatehousemedia.client.viafoura.db.domain.URegistered;
import com.gatehousemedia.client.viafoura.db.domain.URegisteredTotal;
import com.gatehousemedia.client.viafoura.db.domain.USchedule;
import com.gatehousemedia.client.viafoura.db.domain.UTopicsSubscribed;
import com.gatehousemedia.client.viafoura.db.domain.UTopicsUnsubscribed;
import com.gatehousemedia.client.viafoura.db.domain.UVisits;
import com.gatehousemedia.client.viafoura.db.repo.EAnonymousTotalRepo;
import com.gatehousemedia.client.viafoura.db.repo.EBrowserClickedRepo;
import com.gatehousemedia.client.viafoura.db.repo.EBrowserReceivedRepo;
import com.gatehousemedia.client.viafoura.db.repo.EClicksTotalRepo;
import com.gatehousemedia.client.viafoura.db.repo.EJsTotalRepo;
import com.gatehousemedia.client.viafoura.db.repo.EOpensTotalRepo;
import com.gatehousemedia.client.viafoura.db.repo.EVisitorsAttentionRepo;
import com.gatehousemedia.client.viafoura.db.repo.EVisitorsUserAgentsRepo;
import com.gatehousemedia.client.viafoura.db.repo.EVisitorsVisitsRepo;
import com.gatehousemedia.client.viafoura.db.repo.RegisteredDistinctRepo;
import com.gatehousemedia.client.viafoura.db.repo.UAnonymousTotalRepo;
import com.gatehousemedia.client.viafoura.db.repo.UCommentsRepo;
import com.gatehousemedia.client.viafoura.db.repo.UDislikesRepo;
import com.gatehousemedia.client.viafoura.db.repo.UFollowsPagesRepo;
import com.gatehousemedia.client.viafoura.db.repo.UFollowsUsersRepo;
import com.gatehousemedia.client.viafoura.db.repo.ULikesRepo;
import com.gatehousemedia.client.viafoura.db.repo.ULoginsRepo;
import com.gatehousemedia.client.viafoura.db.repo.URatingsRepo;
import com.gatehousemedia.client.viafoura.db.repo.URegisteredRepo;
import com.gatehousemedia.client.viafoura.db.repo.URegisteredTotalRepo;
import com.gatehousemedia.client.viafoura.db.repo.UScheduleRepo;
import com.gatehousemedia.client.viafoura.db.repo.UTopicsSubscribedRepo;
import com.gatehousemedia.client.viafoura.db.repo.UTopicsUnsubscribedRepo;
import com.gatehousemedia.client.viafoura.db.repo.UVisitsRepo;
import com.gatehousemedia.client.viafoura.json.result.bean.EngagementAnonymousTotalResult;
import com.gatehousemedia.client.viafoura.json.result.bean.EngagementBrowser;
import com.gatehousemedia.client.viafoura.json.result.bean.EngagementBrowserClickedResult;
import com.gatehousemedia.client.viafoura.json.result.bean.EngagementBrowserReceivedResult;
import com.gatehousemedia.client.viafoura.json.result.bean.EngagementClicksTotalResult;
import com.gatehousemedia.client.viafoura.json.result.bean.EngagementJSTotalResult;
import com.gatehousemedia.client.viafoura.json.result.bean.EngagementOpensTotalResult;
import com.gatehousemedia.client.viafoura.json.result.bean.EngagementVisitorsAttentionResult;
import com.gatehousemedia.client.viafoura.json.result.bean.EngagementVisitorsUserAgentsResult;
import com.gatehousemedia.client.viafoura.json.result.bean.EngagementVisitorsVisitsResult;
import com.gatehousemedia.client.viafoura.json.result.bean.JsonResult;
import com.gatehousemedia.client.viafoura.json.result.bean.RegisteredUserDetails;
import com.gatehousemedia.client.viafoura.json.result.bean.Social;
import com.gatehousemedia.client.viafoura.json.result.bean.UserAgents;
import com.gatehousemedia.client.viafoura.json.result.bean.UserAnonymousTotalResult;
import com.gatehousemedia.client.viafoura.json.result.bean.UserCommentsResult;
import com.gatehousemedia.client.viafoura.json.result.bean.UserDislikesResult;
import com.gatehousemedia.client.viafoura.json.result.bean.UserFollowsPagesResult;
import com.gatehousemedia.client.viafoura.json.result.bean.UserFollowsUsersResult;
import com.gatehousemedia.client.viafoura.json.result.bean.UserLikesResult;
import com.gatehousemedia.client.viafoura.json.result.bean.UserLoginsResult;
import com.gatehousemedia.client.viafoura.json.result.bean.UserRatingsResult;
import com.gatehousemedia.client.viafoura.json.result.bean.UserRegisteredTotalResult;
import com.gatehousemedia.client.viafoura.json.result.bean.UserScheduleResult;
import com.gatehousemedia.client.viafoura.json.result.bean.UserTopicsSubscribedResult;
import com.gatehousemedia.client.viafoura.json.result.bean.UserTopicsUnsubscribedResult;
import com.gatehousemedia.client.viafoura.json.result.bean.UserVisitsResult;
import com.gatehousemedia.client.viafoura.json.result.bean.ViafouraUser;
import com.gatehousemedia.client.viafoura.json.result.bean.Visits;
import com.gatehousemedia.client.viafoura.util.DirectoryUtils;


@Component(value="viafouraJsonGenerator")
public class ViafouraJsonGenerator {

	@Autowired
	protected EClicksTotalRepo eClicksTotalRepo;
	
	@Autowired
	protected EAnonymousTotalRepo eAnonymousTotalRepo;
	
	@Autowired
	protected EBrowserClickedRepo eBrowserClickedRepo;
	
	@Autowired
	protected EBrowserReceivedRepo eBrowserReceivedRepo;
	
	@Autowired
	protected EJsTotalRepo eJsTotalRepo;
	
	@Autowired
	protected EOpensTotalRepo eOpensTotalRepo;
	
	@Autowired
	protected  EVisitorsAttentionRepo eVisitorsAttentionRepo;
	
	@Autowired
	protected EVisitorsUserAgentsRepo eVisitorsUserAgentsRepo;
	
	@Autowired
	protected EVisitorsVisitsRepo eVisitorsVisitsRepo;
	
	@Autowired
	protected UAnonymousTotalRepo uAnonymousTotalRepo;
	
	@Autowired
	protected UCommentsRepo uCommentsRepo;
	
	@Autowired
	protected UDislikesRepo uDislikesRepo;
	
	@Autowired
	protected UFollowsPagesRepo uFollowsPagesRepo;
	
	@Autowired
	protected UFollowsUsersRepo uFollowsUsersRepo;
	
	@Autowired
	protected ULikesRepo uLikesRepo;	
	
	@Autowired
	protected ULoginsRepo uLoginsRepo;	
	
	@Autowired
	protected URatingsRepo uRatingsRepo;	
	
	@Autowired
	protected URegisteredRepo uRegisteredRepo;	
	
	@Autowired
	protected URegisteredTotalRepo uRegisteredTotalRepo;		
	
	@Autowired
	protected UScheduleRepo uScheduleRepo;	
	
	@Autowired
	protected UTopicsSubscribedRepo uTopicsSubscribedRepo;
	
	@Autowired
	protected UTopicsUnsubscribedRepo uTopicsUnsubscribedRepo;
	
	@Autowired
	protected UVisitsRepo uVisitsRepo;
	
	@Autowired
	protected RegisteredDistinctRepo registeredDistinctRepo;
	
	
	public void run() throws Exception {
		
		PrintWriter fileWriter = new PrintWriter(new BufferedWriter(new FileWriter(PropertyValue.LOCAL_DIRECTORY + "/"+ DirectoryUtils.exportJsonFileName)));
		
		JsonResult jsonResult = new JsonResult();
		
		/*
		 * Table    :  u_anonymous_total
		 * Service  :  /engagement/visitors/anonymous/total 
		 */
		
		Iterable<EAnonymousTotal> eAnonymousTotals = eAnonymousTotalRepo.findAll();
		if(eAnonymousTotals != null){
			for(EAnonymousTotal eAnonymousTotal : eAnonymousTotals){
				EngagementAnonymousTotalResult engagementAnonymousTotalResult = new EngagementAnonymousTotalResult();
				engagementAnonymousTotalResult.setScope(eAnonymousTotal.getScope());
				engagementAnonymousTotalResult.setAggregate(eAnonymousTotal.getAggregate().longValue());
				
				jsonResult.getEngagementAnonymousTotals().add(engagementAnonymousTotalResult);
			}
		}		
		
		/*
		 * Table    :  e_js_total
		 * Service  :  /engagement/loads/js/total
		 */
		Iterable<EJsTotal> eJsTotals = eJsTotalRepo.findAll();
		if(eJsTotals != null){
			for(EJsTotal eJsTotal : eJsTotals){
				EngagementJSTotalResult engagementJSTotalResult = new EngagementJSTotalResult();
				engagementJSTotalResult.setScope(eJsTotal.getScope());
				engagementJSTotalResult.setAggregate(eJsTotal.getAggregate().longValue());
				
				jsonResult.getEngagementJSTotals().add(engagementJSTotalResult);
			}
		}		
		
		/*
		 * Table    :  u_registered_total
		 * Service  :  /users/registered/total
		 */
		Iterable<URegisteredTotal> uRegisteredTotals = uRegisteredTotalRepo.findAll();
		if(uRegisteredTotals != null){
			for(URegisteredTotal uRegisteredTotal : uRegisteredTotals){
				UserRegisteredTotalResult userRegisteredTotalResult = new UserRegisteredTotalResult();
				userRegisteredTotalResult.setScope(uRegisteredTotal.getScope());
				userRegisteredTotalResult.setAggregate(uRegisteredTotal.getAggregate().longValue());
				
				jsonResult.getUserRegisteredTotals().add(userRegisteredTotalResult);
			}
		}		
		
		/*
		 * Table    :  u_anonymous_total
		 * Service  :  /users/anonymous/total
		 */
		Iterable<UAnonymousTotal> uAnonymousTotals = uAnonymousTotalRepo.findAll();
		if(uAnonymousTotals != null){
			for(UAnonymousTotal uAnonymousTotal : uAnonymousTotals){
				UserAnonymousTotalResult userAnonymousTotalResult = new UserAnonymousTotalResult();
				userAnonymousTotalResult.setScope(uAnonymousTotal.getScope());
				userAnonymousTotalResult.setAggregate(uAnonymousTotal.getAggregate().longValue());
				
				jsonResult.getUserAnonymousTotals().add(userAnonymousTotalResult);
			}
		}
		
		
		/*
		 * generating jsons from objects
		 */
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		
		String engagementAnonymousTotalsJson = mapper.writeValueAsString(jsonResult.getEngagementAnonymousTotals());
		String engagementJSTotalsJson = mapper.writeValueAsString(jsonResult.getEngagementJSTotals());
		String userRegisteredTotalsJson = mapper.writeValueAsString(jsonResult.getUserRegisteredTotals());
		String userAnonymousTotalsJson = mapper.writeValueAsString(jsonResult.getUserAnonymousTotals());
		
		StringBuilder sbJsonResult = new StringBuilder();
		
		sbJsonResult.append("{") //start of json
		
		.append(" \"engagementAnonymousTotals\" : ")
		.append(engagementAnonymousTotalsJson)	
		
		.append(",")

		.append(" \"engagementJSTotals\" : ")
		.append(engagementJSTotalsJson)
		
		.append(",")

		.append(" \"userRegisteredTotals\" : ")
		.append(userRegisteredTotalsJson)
		
		.append(",")

		.append(" \"userAnonymousTotals\" : ")
		.append(userAnonymousTotalsJson);	
		
		sbJsonResult.append(",")

		.append(" \"viafouraUsers\" : [");

		long totalDBRecordsCounts = registeredDistinctRepo.count();
		int usersCount = 0;
		
		for(int pagingIndex=0; true ; pagingIndex++){	
			
			Sort sort = new Sort(new Sort.Order(Direction.ASC, "id"));
			Pageable pageable = new PageRequest(pagingIndex, 150, sort);
			
			Page<RegisteredDistinct> registeredDistinctPages =   registeredDistinctRepo.findAll(pageable);
			
			if(registeredDistinctPages.getContent().size() == 0){
				break;
			}else{
				for(RegisteredDistinct registeredDistinct : registeredDistinctPages){
					
					if(usersCount > 0 && usersCount < totalDBRecordsCounts){
						fileWriter.println(",");
						fileWriter.flush();
					}	
					
					usersCount++;
					
					ViafouraUser viafouraUser = new ViafouraUser();					
					viafouraUser.setUser(registeredDistinct.getUuid());
					
					/*  (1)
					 * Table    :  u_registered
					 * Service  :  /users/registered
					 */
					Iterable<URegistered> uRegistereds = uRegisteredRepo.findByUser(registeredDistinct.getUuid());
					if(uRegistereds != null){
						for(URegistered uRegistered : uRegistereds){
							RegisteredUserDetails registeredUserDetails = new RegisteredUserDetails();
							registeredUserDetails.setEmail(uRegistered.getEmail());
							registeredUserDetails.setName(uRegistered.getName());
							registeredUserDetails.setScope(uRegistered.getScope());
							
							
							Social[] socials = mapper.readValue(new String(uRegistered.getSocial()), Social[].class);
							
							registeredUserDetails.setSocials(Arrays.asList(socials));
							
							viafouraUser.getUserDetails().add(registeredUserDetails);
						}
					}
					
					/*  (2)
					 * Table    :  u_ratings
					 * Service  :  /users/ratings
					 */
					Iterable<URatings> uRatingses = uRatingsRepo.findByUser(registeredDistinct.getUuid());
					if(uRatingses != null){
						for(URatings uRatings : uRatingses){
							UserRatingsResult userRatingsResult = new UserRatingsResult();
							userRatingsResult.setScope(uRatings.getScope());
							userRatingsResult.setAggregate(uRatings.getAggregate().longValue());
							
							viafouraUser.getUserRatings().add(userRatingsResult);
						}
					}
					
					/*  (3)
					 * Table    :  u_follows_users
					 * Service  :  /users/follows/users
					 */
					Iterable<UFollowsUsers> uFollowsUserses = uFollowsUsersRepo.findByUser(registeredDistinct.getUuid());
					if(uFollowsUserses != null){
						for(UFollowsUsers uFollowsUsers : uFollowsUserses){
							UserFollowsUsersResult userFollowsUsersResult = new UserFollowsUsersResult();
							userFollowsUsersResult.setScope(uFollowsUsers.getScope());
							userFollowsUsersResult.setAggregate(uFollowsUsers.getAggregate().longValue());
							
							viafouraUser.getUserFollowsUsers().add(userFollowsUsersResult);
						}
					}
					
					/*  (4)
					 * Table    :  u_dislikes
					 * Service  :  /users/dislikes
					 */
					Iterable<UDislikes> uDislikeses = uDislikesRepo.findByUser(registeredDistinct.getUuid());
					if(uDislikeses != null){
						for(UDislikes uDislikes : uDislikeses){
							UserDislikesResult userDislikesResult = new UserDislikesResult();
							userDislikesResult.setScope(uDislikes.getScope());
							userDislikesResult.setAggregate(uDislikes.getAggregate().longValue());
							
							viafouraUser.getUserDislikes().add(userDislikesResult);
						}
					}
					
					/*  (5)
					 * Table    :  e_opens_total
					 * Service  :  /engagement/tray/opens/total
					 */
					Iterable<EOpensTotal> eOpensTotals = eOpensTotalRepo.findByUser(registeredDistinct.getUuid());
					if(eOpensTotals != null){
						for(EOpensTotal eOpensTotal : eOpensTotals){
							EngagementOpensTotalResult engagementOpensTotalResult = new EngagementOpensTotalResult();
							engagementOpensTotalResult.setScope(eOpensTotal.getScope());
							engagementOpensTotalResult.setAggregate(eOpensTotal.getAggregate().longValue());
							
							viafouraUser.getEngagementOpensTotals().add(engagementOpensTotalResult);
						}
					}					
					
					/*  (6)
					 * Table    :  e_opens_total
					 * Service  :  /engagement/tray/opens/total
					 */
					Iterable<UTopicsUnsubscribed> uTopicsUnsubscribeds = uTopicsUnsubscribedRepo.findByUser(registeredDistinct.getUuid());
					if(uTopicsUnsubscribeds != null){
						for(UTopicsUnsubscribed uTopicsUnsubscribed : uTopicsUnsubscribeds){
							UserTopicsUnsubscribedResult userTopicsUnsubscribedResult = new UserTopicsUnsubscribedResult();
							userTopicsUnsubscribedResult.setScope(uTopicsUnsubscribed.getScope());
							
							
							
							userTopicsUnsubscribedResult.setTopics(new String(uTopicsUnsubscribed.getTopics()));
							
							viafouraUser.getUserTopicsUnsubscribeds().add(userTopicsUnsubscribedResult);
						}
					}					
					
					/*  (7)
					 * Table    :  e_visitors_attention
					 * Service  :  /engagement/visitors/attention
					 */
					Iterable<EVisitorsAttention> eVisitorsAttentions = eVisitorsAttentionRepo.findByUser(registeredDistinct.getUuid());
					if(eVisitorsAttentions != null){
						for(EVisitorsAttention eVisitorsAttention : eVisitorsAttentions){
							EngagementVisitorsAttentionResult engagementVisitorsAttentionResult = new EngagementVisitorsAttentionResult();
							engagementVisitorsAttentionResult.setScope(eVisitorsAttention.getScope());
							engagementVisitorsAttentionResult.setAggregate(eVisitorsAttention.getAggregate().longValue());
							
							viafouraUser.getEngagementVisitorsAttentions().add(engagementVisitorsAttentionResult);
						}
					}	
					
					/*  (8)
					 * Table    :  u_likes
					 * Service  :  /users/likes
					 */
					Iterable<ULikes> uLikeses = uLikesRepo.findByUser(registeredDistinct.getUuid());
					if(uLikeses != null){
						for(ULikes uLikes : uLikeses){
							UserLikesResult userLikesResult = new UserLikesResult();
							userLikesResult.setScope(uLikes.getScope());
							userLikesResult.setAggregate(uLikes.getAggregate().longValue());
							
							viafouraUser.getUserLikes().add(userLikesResult);
						}
					}					
					
					/*  (9)
					 * Table    :  e_browser_clicked
					 * Service  :  /engagement/notifications/browser/clicked
					 */
					Iterable<EBrowserClicked> eBrowserClickeds = eBrowserClickedRepo.findByUser(registeredDistinct.getUuid());
					if(eBrowserClickeds != null){						
						EngagementBrowserClickedResult engagementBrowserClickedResult = new EngagementBrowserClickedResult();
						for(EBrowserClicked eBrowserClicked : eBrowserClickeds){							
							EngagementBrowser engagementBrowser = new EngagementBrowser();
							engagementBrowser.setScope(eBrowserClicked.getScope());
							engagementBrowser.setBroadcast(eBrowserClicked.getBroadcast().longValue());
							engagementBrowser.setReply(eBrowserClicked.getReply().longValue());
							engagementBrowser.setTopics(eBrowserClicked.getTopic().longValue());
							
							if(eBrowserClicked.getIsRegistered()){
								engagementBrowserClickedResult.getRegistered().add(engagementBrowser);
							}else{
								engagementBrowserClickedResult.getAnonymous().add(engagementBrowser);
							}
							
							viafouraUser.getEngagementBrowserClickeds().add(engagementBrowserClickedResult);
						}
					}
					
					/*  (10)
					 * Table    :  e_browser_received
					 * Service  :  /engagement/notifications/browser/received
					 */
					Iterable<EBrowserReceived> eBrowserReceiveds = eBrowserReceivedRepo.findByUser(registeredDistinct.getUuid());
					if(eBrowserReceiveds != null){						
						EngagementBrowserReceivedResult engagementBrowserReceivedResult = new EngagementBrowserReceivedResult();
						for(EBrowserReceived eBrowserReceived : eBrowserReceiveds){							
							EngagementBrowser engagementBrowser = new EngagementBrowser();
							engagementBrowser.setScope(eBrowserReceived.getScope());
							engagementBrowser.setBroadcast(eBrowserReceived.getBroadcast().longValue());
							engagementBrowser.setReply(eBrowserReceived.getReply().longValue());
							engagementBrowser.setTopics(eBrowserReceived.getTopic().longValue());
							
							if(eBrowserReceived.getIsRegistered()){
								engagementBrowserReceivedResult.getRegistered().add(engagementBrowser);
							}else{
								engagementBrowserReceivedResult.getAnonymous().add(engagementBrowser);
							}
							
							viafouraUser.getEngagementBrowserReceiveds().add(engagementBrowserReceivedResult);
						}
					}
					
					/*  (11)
					 * Table    :  e_visitors_user_agents
					 * Service  :  /engagement/visitors/user_agents
					 */
					Iterable<EVisitorsUserAgents> eVisitorsUserAgentses = eVisitorsUserAgentsRepo.findByUser(registeredDistinct.getUuid());
					if(eVisitorsUserAgentses != null){
						for(EVisitorsUserAgents eVisitorsUserAgents : eVisitorsUserAgentses){
							EngagementVisitorsUserAgentsResult engagementVisitorsUserAgentsResult = new EngagementVisitorsUserAgentsResult();
							engagementVisitorsUserAgentsResult.setScope(eVisitorsUserAgents.getScope());
							
							UserAgents[] userAgentses = mapper.readValue(new String(eVisitorsUserAgents.getUserAgents()), UserAgents[].class);							
							engagementVisitorsUserAgentsResult.setUserAgents(Arrays.asList(userAgentses));
							
							viafouraUser.getEngagementVisitorsUserAgents().add(engagementVisitorsUserAgentsResult);
						}
					}
					
					/*  (12)
					 * Table    :  u_topics_subscribed
					 * Service  :  /users/topics/subscribed
					 */
					Iterable<UTopicsSubscribed> uTopicsSubscribeds = uTopicsSubscribedRepo.findByUser(registeredDistinct.getUuid());
					if(uTopicsSubscribeds != null){
						for(UTopicsSubscribed uTopicsSubscribed : uTopicsSubscribeds){
							UserTopicsSubscribedResult userTopicsSubscribedResult = new UserTopicsSubscribedResult();
							userTopicsSubscribedResult.setScope(uTopicsSubscribed.getScope());
							userTopicsSubscribedResult.setTopics(new String(uTopicsSubscribed.getTopics()));
							
							viafouraUser.getUserTopicsSubscribeds().add(userTopicsSubscribedResult);
						}
					}					
					
					/*  (13)
					 * Table    :  e_visitors_visits
					 * Service  :  /engagement/visitors/visits
					 */
					Iterable<EVisitorsVisits> eVisitorsVisitses = eVisitorsVisitsRepo.findByUser(registeredDistinct.getUuid());
					if(eVisitorsVisitses != null){
						for(EVisitorsVisits eVisitorsVisits : eVisitorsVisitses){
							EngagementVisitorsVisitsResult engagementVisitorsVisitsResult = new EngagementVisitorsVisitsResult();
							engagementVisitorsVisitsResult.setScope(eVisitorsVisits.getScope());
							
							Visits[] Visitses = mapper.readValue(new String(eVisitorsVisits.getVisits()), Visits[].class);
							engagementVisitorsVisitsResult.setVisits(Arrays.asList(Visitses));
							
							viafouraUser.getEngagementVisitorsVisits().add(engagementVisitorsVisitsResult);
						}
					}	
					
					/*  (14)
					 * Table    :  u_schedule
					 * Service  :  /users/schedule
					 */
					Iterable<USchedule> uSchedules = uScheduleRepo.findByUser(registeredDistinct.getUuid());
					if(uSchedules != null){
						for(USchedule uSchedule : uSchedules){
							UserScheduleResult userScheduleResult = new UserScheduleResult();
							userScheduleResult.setScope(uSchedule.getScope());
							
							Long[] viewTimestampses = mapper.readValue(new String(uSchedule.getViewTimestamps()), Long[].class);
							userScheduleResult.setViewTimestamps(Arrays.asList(viewTimestampses));
							
							viafouraUser.getUserSchedule().add(userScheduleResult);
						}
					}					
					
					/*  (15)
					 * Table    :  e_clicks_total
					 * Service  :  /engagement/indicator/clicks/total
					 */
					Iterable<EClicksTotal> eClicksTotals = eClicksTotalRepo.findByUser(registeredDistinct.getUuid());
					if(eClicksTotals != null){
						for(EClicksTotal eClicksTotal : eClicksTotals){
							EngagementClicksTotalResult engagementClicksTotalResult = new EngagementClicksTotalResult();
							engagementClicksTotalResult.setScope(eClicksTotal.getScope());
							engagementClicksTotalResult.setAggregate(eClicksTotal.getAggregate().longValue());
							
							viafouraUser.getEngagementClicksTotals().add(engagementClicksTotalResult);
						}
					}
					
					/*  (16)
					 * Table    :  u_comments
					 * Service  :  /users/comments
					 */
					Iterable<UComments> uCommentses = uCommentsRepo.findByUser(registeredDistinct.getUuid());
					if(uCommentses != null){
						for(UComments uComments : uCommentses){
							UserCommentsResult userCommentsResult = new UserCommentsResult();
							userCommentsResult.setScope(uComments.getScope());
							userCommentsResult.setAggregate(uComments.getAggregate().longValue());
							
							viafouraUser.getUserComments().add(userCommentsResult);
						}
					}					
					
					/*  (17)
					 * Table    :  u_logins
					 * Service  :  /users/logins
					 */
					Iterable<ULogins> uLoginses = uLoginsRepo.findByUser(registeredDistinct.getUuid());
					if(uLoginses != null){
						for(ULogins uLogins : uLoginses){
							UserLoginsResult userLoginsResult = new UserLoginsResult();
							userLoginsResult.setScope(uLogins.getScope());
							userLoginsResult.setAggregate(uLogins.getAggregate().longValue());
							
							viafouraUser.getUserLogins().add(userLoginsResult);
						}
					}
					
					/*  (18)
					 * Table    :  u_visits
					 * Service  :  /users/visits
					 */
					Iterable<UVisits> uVisitses = uVisitsRepo.findByUser(registeredDistinct.getUuid());
					if(uVisitses != null){
						for(UVisits uVisits : uVisitses){
							UserVisitsResult userVisitsResult = new UserVisitsResult();
							userVisitsResult.setScope(uVisits.getScope());
							userVisitsResult.setAggregate(uVisits.getAggregate().longValue());
							
							viafouraUser.getUserVisits().add(userVisitsResult);
						}
					}	
					
					/*  (19)
					 * Table    :  u_follows_pages
					 * Service  :  /users/follows/pages
					 */
					Iterable<UFollowsPages> uFollowsPageses = uFollowsPagesRepo.findByUser(registeredDistinct.getUuid());
					if(uFollowsPageses != null){
						for(UFollowsPages uFollowsPages : uFollowsPageses){
							UserFollowsPagesResult userFollowsPagesResult = new UserFollowsPagesResult();
							userFollowsPagesResult.setScope(uFollowsPages.getScope());
							userFollowsPagesResult.setAggregate(uFollowsPages.getAggregate().longValue());
							
							viafouraUser.getUserFollowsPages().add(userFollowsPagesResult);
						}
					}					
				
					
					String viafouraUserJson = mapper.writeValueAsString(viafouraUser);					
					sbJsonResult.append(viafouraUserJson);
					
					fileWriter.println(sbJsonResult.toString());
					sbJsonResult = new StringBuilder();
					fileWriter.flush();
				}
				
				
			}

		}
		
		System.out.println("________________________________________________________________END________________________________________________________________");
		
		sbJsonResult.append("]");
		
		sbJsonResult.append("}"); // end of json
		
		fileWriter.println(sbJsonResult.toString());
		
		fileWriter.flush();
		fileWriter.close();
	}
	
	
	
	
	
}
