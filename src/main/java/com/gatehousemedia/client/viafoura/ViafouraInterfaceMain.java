package com.gatehousemedia.client.viafoura;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.gatehousemedia.client.viafoura.constant.PropertyValue;
import com.gatehousemedia.client.viafoura.service.ViafouraHttpServiceRange;
import com.gatehousemedia.client.viafoura.util.LoggingUtils;
import com.gatehousemedia.client.viafoura.util.PropertyLoader;

public class ViafouraInterfaceMain {	
	
	public static boolean enableSchedule = true;

	public static void main(String[] args) {		
		try{
			PropertyLoader.loadApplicationProperties();
			
	    	/*
	    	 * if the cron-expression exist then schedule the export according to expression
	    	 * otherwise run the export only once
	    	 */
			if(PropertyValue.CRON_EXPRESSION != null &&  !"1 0 0 0 0 0".equals(PropertyValue.CRON_EXPRESSION.trim())){
				PropertyValue.CRON_EXPRESSION = PropertyValue.CRON_EXPRESSION.trim();
				
				LoggingUtils.logInfo(PropertyValue.CRON_EXPRESSION);
				
				@SuppressWarnings({ "unused", "resource" })
				ApplicationContext ctx = new AnnotationConfigApplicationContext(ViafouraInterfaceConfig.class, ViafouraAPIScheduler.class);	
			}else{
			
				enableSchedule = false;
				
				@SuppressWarnings({ "resource" })
				ApplicationContext ctx = new AnnotationConfigApplicationContext(ViafouraInterfaceConfig.class);	
				ViafouraHttpServiceRange viafouraClient = (ViafouraHttpServiceRange)ctx.getBean("viafouraHttpServiceRange");
				viafouraClient.run();
			}	
		
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
		}
	
	}
}
