package com.gatehousemedia.client.viafoura.json.bean;

import java.io.Serializable;
import java.util.List;

public class UVisitsBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 344137065321191689L;
	
	private String scope; // added by application
	private List<UserAggregatePair> result;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public List<UserAggregatePair> getResult() {
		return result;
	}
	public void setResult(List<UserAggregatePair> result) {
		this.result = result;
	}
}
