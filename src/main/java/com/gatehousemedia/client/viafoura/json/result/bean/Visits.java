package com.gatehousemedia.client.viafoura.json.result.bean;

import java.io.Serializable;
import java.util.List;

public class Visits implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 330746200943135439L;
	private List<Long> timestamps; 
	private String path;
	public List<Long> getTimestamps() {
		return timestamps;
	}
	public void setTimestamps(List<Long> timestamps) {
		this.timestamps = timestamps;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	
}
