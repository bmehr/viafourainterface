package com.gatehousemedia.client.viafoura.json.bean;

import java.io.Serializable;

public class EAnonymousTotalBean implements Serializable{	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7701013957970713842L;
	
	private String scope;
	private long  aggregate;	
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public long getAggregate() {
		return aggregate;
	}
	public void setAggregate(long aggregate) {
		this.aggregate = aggregate;
	}
}
