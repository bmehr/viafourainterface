package com.gatehousemedia.client.viafoura.json.bean;

import java.util.List;

public class UserAgent {
	private List<Long> timestamps;
	private String user_agent_string;
	
	public List<Long> getTimestamps() {
		return timestamps;
	}
	public void setTimestamps(List<Long> timestamps) {
		this.timestamps = timestamps;
	}
	public String getUser_agent_string() {
		return user_agent_string;
	}
	public void setUser_agent_string(String user_agent_string) {
		this.user_agent_string = user_agent_string;
	}
	
}
