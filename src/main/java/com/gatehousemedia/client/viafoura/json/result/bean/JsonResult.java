package com.gatehousemedia.client.viafoura.json.result.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class JsonResult implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6142324671374024532L;
	
	private List<EngagementAnonymousTotalResult> engagementAnonymousTotals = new ArrayList<EngagementAnonymousTotalResult>();
	private List<EngagementJSTotalResult> engagementJSTotals = new ArrayList<EngagementJSTotalResult>();
	private List<UserRegisteredTotalResult> userRegisteredTotals = new ArrayList<UserRegisteredTotalResult>();
	private List<UserAnonymousTotalResult> userAnonymousTotals = new ArrayList<UserAnonymousTotalResult>();	
	private List<ViafouraUser> viafouraUsers = new ArrayList<ViafouraUser>();
	
	
	public List<EngagementAnonymousTotalResult> getEngagementAnonymousTotals() {
		return engagementAnonymousTotals;
	}
	public void setEngagementAnonymousTotals(List<EngagementAnonymousTotalResult> engagementAnonymousTotals) {
		this.engagementAnonymousTotals = engagementAnonymousTotals;
	}
	public List<EngagementJSTotalResult> getEngagementJSTotals() {
		return engagementJSTotals;
	}
	public void setEngagementJSTotals(List<EngagementJSTotalResult> engagementJSTotals) {
		this.engagementJSTotals = engagementJSTotals;
	}
	public List<UserRegisteredTotalResult> getUserRegisteredTotals() {
		return userRegisteredTotals;
	}
	public void setUserRegisteredTotals(List<UserRegisteredTotalResult> userRegisteredTotals) {
		this.userRegisteredTotals = userRegisteredTotals;
	}
	public List<UserAnonymousTotalResult> getUserAnonymousTotals() {
		return userAnonymousTotals;
	}
	public void setUserAnonymousTotals(List<UserAnonymousTotalResult> userAnonymousTotals) {
		this.userAnonymousTotals = userAnonymousTotals;
	}
	public List<ViafouraUser> getViafouraUsers() {
		return viafouraUsers;
	}
	public void setViafouraUsers(List<ViafouraUser> viafouraUsers) {
		this.viafouraUsers = viafouraUsers;
	}
	

}
