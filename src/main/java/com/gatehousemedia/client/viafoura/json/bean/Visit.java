package com.gatehousemedia.client.viafoura.json.bean;

import java.util.List;

public class Visit {

	private List<Long> timestamps;
	private String path;
	
	public List<Long> getTimestamps() {
		return timestamps;
	}
	public void setTimestamps(List<Long> timestamps) {
		this.timestamps = timestamps;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
}
