package com.gatehousemedia.client.viafoura.json.bean;

import java.io.Serializable;
import java.util.List;

public class UTopicsUnsubscribedBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2098243728791528869L;
	
	private String scope; // added by application
	private List<UserTopicsPair> result;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public List<UserTopicsPair> getResult() {
		return result;
	}
	public void setResult(List<UserTopicsPair> result) {
		this.result = result;
	}
}
