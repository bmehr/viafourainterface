package com.gatehousemedia.client.viafoura.json.result.bean;

import java.util.List;

public class UserScheduleResult {

	private String scope;
	private List<Long> viewTimestamps;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public List<Long> getViewTimestamps() {
		return viewTimestamps;
	}
	public void setViewTimestamps(List<Long> viewTimestamps) {
		this.viewTimestamps = viewTimestamps;
	}
	
}
