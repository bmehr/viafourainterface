package com.gatehousemedia.client.viafoura.json.bean;

import java.io.Serializable;
import java.util.List;

public class EVisitorsVisitsBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8714465751141421681L;
	
	
	private String scope;
	private List<UserVisitsPair> result;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public List<UserVisitsPair> getResult() {
		return result;
	}
	public void setResult(List<UserVisitsPair> result) {
		this.result = result;
	}

}
