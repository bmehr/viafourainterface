package com.gatehousemedia.client.viafoura.json.bean;

import java.io.Serializable;
import java.util.List;

public class EBrowserReceivedBean implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7956442193020607275L;
	
	private String scope; //added by application
	private List<UserNotificationAggregates> registered;
	private List<UserNotificationAggregates> anonymous;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public List<UserNotificationAggregates> getRegistered() {
		return registered;
	}
	public void setRegistered(List<UserNotificationAggregates> registered) {
		this.registered = registered;
	}
	public List<UserNotificationAggregates> getAnonymous() {
		return anonymous;
	}
	public void setAnonymous(List<UserNotificationAggregates> anonymous) {
		this.anonymous = anonymous;
	}
}
