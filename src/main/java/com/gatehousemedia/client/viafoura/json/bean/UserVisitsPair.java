package com.gatehousemedia.client.viafoura.json.bean;

import java.util.List;

public class UserVisitsPair {
	
	private String uuid;
	private List<Visit> visits;
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public List<Visit> getVisits() {
		return visits;
	}
	public void setVisits(List<Visit> visits) {
		this.visits = visits;
	}

}
