package com.gatehousemedia.client.viafoura.json.bean;

import java.io.Serializable;

public class UAnonymousTotalBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3813150104821694971L;
	
	private String scope;
	private long  aggregate;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public long getAggregate() {
		return aggregate;
	}
	public void setAggregate(long aggregate) {
		this.aggregate = aggregate;
	}	

}
