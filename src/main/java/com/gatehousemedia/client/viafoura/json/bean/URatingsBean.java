package com.gatehousemedia.client.viafoura.json.bean;

import java.io.Serializable;
import java.util.List;

public class URatingsBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7192106945847773581L;
	
	private String scope; // added by application
	private List<UserAggregatePair> result;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public List<UserAggregatePair> getResult() {
		return result;
	}
	public void setResult(List<UserAggregatePair> result) {
		this.result = result;
	}

}
