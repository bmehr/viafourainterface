package com.gatehousemedia.client.viafoura.json.bean;

import java.util.List;

public class UserTopicsPair {
	
	private String user;
	private List<String> topics;
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public List<String> getTopics() {
		return topics;
	}
	public void setTopics(List<String> topics) {
		this.topics = topics;
	}	
}
