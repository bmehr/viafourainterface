package com.gatehousemedia.client.viafoura.json.bean;

import java.io.Serializable;
import java.util.List;

public class EVisitorsUserAgentsBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -450430310024011930L;
	
	private String scope;
	private List<UserUserAgentsPair> result;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public List<UserUserAgentsPair> getResult() {
		return result;
	}
	public void setResult(List<UserUserAgentsPair> result) {
		this.result = result;
	}
	

}
