package com.gatehousemedia.client.viafoura.json.result.bean;

import java.util.ArrayList;
import java.util.List;

public class EngagementBrowserClickedResult {

	private List<EngagementBrowser> registered = new ArrayList<EngagementBrowser>();
	private List<EngagementBrowser> anonymous = new ArrayList<EngagementBrowser>();
		
	public List<EngagementBrowser> getRegistered() {
		return registered;
	}
	public void setRegistered(List<EngagementBrowser> registered) {
		this.registered = registered;
	}
	public List<EngagementBrowser> getAnonymous() {
		return anonymous;
	}
	public void setAnonymous(List<EngagementBrowser> anonymous) {
		this.anonymous = anonymous;
	}
	
	
}
