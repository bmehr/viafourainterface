package com.gatehousemedia.client.viafoura.json.bean;

import java.io.Serializable;
import java.util.List;

public class URegisteredBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6921729063086630021L;
	
	private String scope; // added by application
	private List<AddressableUser> result;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public List<AddressableUser> getResult() {
		return result;
	}
	public void setResult(List<AddressableUser> result) {
		this.result = result;
	}
}
