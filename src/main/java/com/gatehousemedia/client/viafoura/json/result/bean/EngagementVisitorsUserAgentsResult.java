package com.gatehousemedia.client.viafoura.json.result.bean;

import java.util.List;

public class EngagementVisitorsUserAgentsResult {
	
	private String scope;
	private List<UserAgents> userAgents;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public List<UserAgents> getUserAgents() {
		return userAgents;
	}
	public void setUserAgents(List<UserAgents> userAgents) {
		this.userAgents = userAgents;
	}
	
}
