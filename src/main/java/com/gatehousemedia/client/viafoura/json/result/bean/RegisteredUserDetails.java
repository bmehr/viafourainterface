package com.gatehousemedia.client.viafoura.json.result.bean;

import java.util.List;

public class RegisteredUserDetails {
	
	private String scope;
	private String name;
	private String email;
	private List<Social> socials;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Social> getSocials() {
		return socials;
	}
	public void setSocials(List<Social> socials) {
		this.socials = socials;
	}
	

}
