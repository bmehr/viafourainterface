package com.gatehousemedia.client.viafoura.json.bean;

import java.util.List;

public class UserUserAgentsPair {
	private String user; //uuid
	private List<UserAgent> user_agents;
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public List<UserAgent> getUser_agents() {
		return user_agents;
	}
	public void setUser_agents(List<UserAgent> user_agents) {
		this.user_agents = user_agents;
	}
	
}
