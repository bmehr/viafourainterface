package com.gatehousemedia.client.viafoura.json.bean;

import java.util.List;

public class UserTimestampsPair {
	
	private String user; // uuid
	private List<Long> view_timestamps;
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public List<Long> getView_timestamps() {
		return view_timestamps;
	}
	public void setView_timestamps(List<Long> view_timestamps) {
		this.view_timestamps = view_timestamps;
	}
	
}
