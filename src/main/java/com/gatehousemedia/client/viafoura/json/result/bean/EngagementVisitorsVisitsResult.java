package com.gatehousemedia.client.viafoura.json.result.bean;

import java.util.List;

public class EngagementVisitorsVisitsResult {
	
	private String scope;
	private List<Visits> visits;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public List<Visits> getVisits() {
		return visits;
	}
	public void setVisits(List<Visits> visits) {
		this.visits = visits;
	}
	

}
