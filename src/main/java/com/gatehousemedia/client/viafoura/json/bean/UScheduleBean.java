package com.gatehousemedia.client.viafoura.json.bean;

import java.io.Serializable;
import java.util.List;

public class UScheduleBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5754123372264841366L;
	
	private String scope; // added by application
	private List<UserTimestampsPair> result;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public List<UserTimestampsPair> getResult() {
		return result;
	}
	public void setResult(List<UserTimestampsPair> result) {
		this.result = result;
	}
}
