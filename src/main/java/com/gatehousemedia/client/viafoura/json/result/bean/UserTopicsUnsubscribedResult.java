package com.gatehousemedia.client.viafoura.json.result.bean;

public class UserTopicsUnsubscribedResult {
	
	private String scope;
	private String topics;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public String getTopics() {
		return topics;
	}
	public void setTopics(String topics) {
		this.topics = topics;
	}

}
