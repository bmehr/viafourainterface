package com.gatehousemedia.client.viafoura.json.result.bean;

public class EngagementBrowser {
	
	private String scope;
	private long broadcast;
	private long reply;
	private long topics;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public long getBroadcast() {
		return broadcast;
	}
	public void setBroadcast(long broadcast) {
		this.broadcast = broadcast;
	}
	public long getReply() {
		return reply;
	}
	public void setReply(long reply) {
		this.reply = reply;
	}
	public long getTopics() {
		return topics;
	}
	public void setTopics(long topics) {
		this.topics = topics;
	}
	
	
	
}
