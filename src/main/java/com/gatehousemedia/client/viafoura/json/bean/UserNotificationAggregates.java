package com.gatehousemedia.client.viafoura.json.bean;

public class UserNotificationAggregates {

	private String uuid;
	private long broadcast;
	private long reply;
	private long topic;
	
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public long getBroadcast() {
		return broadcast;
	}
	public void setBroadcast(long broadcast) {
		this.broadcast = broadcast;
	}
	public long getReply() {
		return reply;
	}
	public void setReply(long reply) {
		this.reply = reply;
	}
	public long getTopic() {
		return topic;
	}
	public void setTopic(long topic) {
		this.topic = topic;
	}
	
	
	
}
