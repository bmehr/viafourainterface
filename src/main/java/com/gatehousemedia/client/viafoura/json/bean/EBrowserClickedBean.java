package com.gatehousemedia.client.viafoura.json.bean;

import java.io.Serializable;
import java.util.List;

public class EBrowserClickedBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4429934136599303016L;

	private String scope; //added by application
	private List<UserNotificationAggregates> registered;
	private List<UserNotificationAggregates> anonymous;
	
	public List<UserNotificationAggregates> getRegistered() {
		return registered;
	}
	public void setRegistered(List<UserNotificationAggregates> registered) {
		this.registered = registered;
	}
	public List<UserNotificationAggregates> getAnonymous() {
		return anonymous;
	}
	public void setAnonymous(List<UserNotificationAggregates> anonymous) {
		this.anonymous = anonymous;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	
}
