package com.gatehousemedia.client.viafoura.json.bean;

import java.util.List;

public class AddressableUser {
	
	private String uuid;
	private String name;
	private String email;
	private List<Social> social;
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Social> getSocial() {
		return social;
	}
	public void setSocial(List<Social> social) {
		this.social = social;
	}
	
}
