package com.gatehousemedia.client.viafoura.json.result.bean;

import java.io.Serializable;
import java.util.List;

public class UserAgents implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2436441747670459792L;
	private List<Long> timestamps;
	private String user_agent_string;
	

	public String getUser_agent_string() {
		return user_agent_string;
	}
	public void setUser_agent_string(String user_agent_string) {
		this.user_agent_string = user_agent_string;
	}
	public List<Long> getTimestamps() {
		return timestamps;
	}
	public void setTimestamps(List<Long> timestamps) {
		this.timestamps = timestamps;
	}
}
