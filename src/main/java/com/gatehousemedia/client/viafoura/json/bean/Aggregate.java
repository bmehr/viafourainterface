package com.gatehousemedia.client.viafoura.json.bean;

public class Aggregate {
	
	private String scope;
	private long  aggregate;
	
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public long getAggregate() {
		return aggregate;
	}
	public void setAggregate(long aggregate) {
		this.aggregate = aggregate;
	}
	
}
