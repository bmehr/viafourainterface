package com.gatehousemedia.client.viafoura.json.result.bean;

import java.io.Serializable;

public class Social implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6999172471777171562L;
	private String provider;
	private String id;
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
}
