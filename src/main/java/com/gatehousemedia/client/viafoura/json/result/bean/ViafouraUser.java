package com.gatehousemedia.client.viafoura.json.result.bean;

import java.util.ArrayList;
import java.util.List;

public class ViafouraUser {

	private String user;
	private List<RegisteredUserDetails> userDetails = new ArrayList<RegisteredUserDetails>();
	private List<UserRatingsResult> userRatings = new ArrayList<UserRatingsResult>();
	private List<UserFollowsUsersResult> userFollowsUsers = new ArrayList<UserFollowsUsersResult>();
	private List<UserDislikesResult> userDislikes = new ArrayList<UserDislikesResult>();	
	private List<UserLikesResult> userLikes = new ArrayList<UserLikesResult>();	
	private List<EngagementOpensTotalResult> engagementOpensTotals = new ArrayList<EngagementOpensTotalResult>();
	private List<EngagementVisitorsAttentionResult> engagementVisitorsAttentions = new ArrayList<EngagementVisitorsAttentionResult>();
	private List<EngagementBrowserClickedResult> engagementBrowserClickeds = new ArrayList<EngagementBrowserClickedResult>();
	private List<EngagementBrowserReceivedResult> engagementBrowserReceiveds = new ArrayList<EngagementBrowserReceivedResult>();
	private List<EngagementVisitorsUserAgentsResult> engagementVisitorsUserAgents = new ArrayList<EngagementVisitorsUserAgentsResult>();
	private List<UserTopicsSubscribedResult> userTopicsSubscribeds = new ArrayList<UserTopicsSubscribedResult>();
	private List<UserTopicsUnsubscribedResult> userTopicsUnsubscribeds = new ArrayList<UserTopicsUnsubscribedResult>();	
	private List<EngagementClicksTotalResult> engagementClicksTotals = new ArrayList<EngagementClicksTotalResult>();
	private List<EngagementVisitorsVisitsResult> engagementVisitorsVisits = new ArrayList<EngagementVisitorsVisitsResult>();
	private List<UserCommentsResult> userComments = new ArrayList<UserCommentsResult>();
	private List<UserLoginsResult> userLogins = new ArrayList<UserLoginsResult>();
	private List<UserVisitsResult> userVisits = new ArrayList<UserVisitsResult>();
	private List<UserScheduleResult> userSchedule = new ArrayList<UserScheduleResult>();
	private List<UserFollowsPagesResult> userFollowsPages = new ArrayList<UserFollowsPagesResult>();
	
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public List<RegisteredUserDetails> getUserDetails() {
		return userDetails;
	}
	public void setUserDetails(List<RegisteredUserDetails> userDetails) {
		this.userDetails = userDetails;
	}
	public List<UserRatingsResult> getUserRatings() {
		return userRatings;
	}
	public void setUserRatings(List<UserRatingsResult> userRatings) {
		this.userRatings = userRatings;
	}
	public List<UserFollowsUsersResult> getUserFollowsUsers() {
		return userFollowsUsers;
	}
	public void setUserFollowsUsers(List<UserFollowsUsersResult> userFollowsUsers) {
		this.userFollowsUsers = userFollowsUsers;
	}
	public List<UserDislikesResult> getUserDislikes() {
		return userDislikes;
	}
	public void setUserDislikes(List<UserDislikesResult> userDislikes) {
		this.userDislikes = userDislikes;
	}
	public List<EngagementOpensTotalResult> getEngagementOpensTotals() {
		return engagementOpensTotals;
	}
	public void setEngagementOpensTotals(List<EngagementOpensTotalResult> engagementOpensTotals) {
		this.engagementOpensTotals = engagementOpensTotals;
	}
	public List<EngagementVisitorsAttentionResult> getEngagementVisitorsAttentions() {
		return engagementVisitorsAttentions;
	}
	public void setEngagementVisitorsAttentions(List<EngagementVisitorsAttentionResult> engagementVisitorsAttentions) {
		this.engagementVisitorsAttentions = engagementVisitorsAttentions;
	}
	public List<EngagementBrowserClickedResult> getEngagementBrowserClickeds() {
		return engagementBrowserClickeds;
	}
	public void setEngagementBrowserClickeds(List<EngagementBrowserClickedResult> engagementBrowserClickeds) {
		this.engagementBrowserClickeds = engagementBrowserClickeds;
	}
	public List<EngagementBrowserReceivedResult> getEngagementBrowserReceiveds() {
		return engagementBrowserReceiveds;
	}
	public void setEngagementBrowserReceiveds(List<EngagementBrowserReceivedResult> engagementBrowserReceiveds) {
		this.engagementBrowserReceiveds = engagementBrowserReceiveds;
	}
	public List<EngagementVisitorsUserAgentsResult> getEngagementVisitorsUserAgents() {
		return engagementVisitorsUserAgents;
	}
	public void setEngagementVisitorsUserAgents(List<EngagementVisitorsUserAgentsResult> engagementVisitorsUserAgents) {
		this.engagementVisitorsUserAgents = engagementVisitorsUserAgents;
	}
	public List<UserTopicsSubscribedResult> getUserTopicsSubscribeds() {
		return userTopicsSubscribeds;
	}
	public void setUserTopicsSubscribeds(List<UserTopicsSubscribedResult> userTopicsSubscribeds) {
		this.userTopicsSubscribeds = userTopicsSubscribeds;
	}
	public List<UserTopicsUnsubscribedResult> getUserTopicsUnsubscribeds() {
		return userTopicsUnsubscribeds;
	}
	public void setUserTopicsUnsubscribeds(List<UserTopicsUnsubscribedResult> userTopicsUnsubscribeds) {
		this.userTopicsUnsubscribeds = userTopicsUnsubscribeds;
	}
	public List<EngagementClicksTotalResult> getEngagementClicksTotals() {
		return engagementClicksTotals;
	}
	public void setEngagementClicksTotals(List<EngagementClicksTotalResult> engagementClicksTotals) {
		this.engagementClicksTotals = engagementClicksTotals;
	}
	public List<EngagementVisitorsVisitsResult> getEngagementVisitorsVisits() {
		return engagementVisitorsVisits;
	}
	public void setEngagementVisitorsVisits(List<EngagementVisitorsVisitsResult> engagementVisitorsVisits) {
		this.engagementVisitorsVisits = engagementVisitorsVisits;
	}
	public List<UserCommentsResult> getUserComments() {
		return userComments;
	}
	public void setUserComments(List<UserCommentsResult> userComments) {
		this.userComments = userComments;
	}
	public List<UserLoginsResult> getUserLogins() {
		return userLogins;
	}
	public void setUserLogins(List<UserLoginsResult> userLogins) {
		this.userLogins = userLogins;
	}
	public List<UserVisitsResult> getUserVisits() {
		return userVisits;
	}
	public void setUserVisits(List<UserVisitsResult> userVisits) {
		this.userVisits = userVisits;
	}
	public List<UserScheduleResult> getUserSchedule() {
		return userSchedule;
	}
	public void setUserSchedule(List<UserScheduleResult> userSchedule) {
		this.userSchedule = userSchedule;
	}
	public List<UserFollowsPagesResult> getUserFollowsPages() {
		return userFollowsPages;
	}
	public void setUserFollowsPages(List<UserFollowsPagesResult> userFollowsPages) {
		this.userFollowsPages = userFollowsPages;
	}
	public List<UserLikesResult> getUserLikes() {
		return userLikes;
	}
	public void setUserLikes(List<UserLikesResult> userLikes) {
		this.userLikes = userLikes;
	}
	

	
	
}
