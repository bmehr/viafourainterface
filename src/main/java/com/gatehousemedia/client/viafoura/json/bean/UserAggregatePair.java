package com.gatehousemedia.client.viafoura.json.bean;

public class UserAggregatePair {
	
	private String user; //uuid
	private long aggregate;
	
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public long getAggregate() {
		return aggregate;
	}
	public void setAggregate(long aggregate) {
		this.aggregate = aggregate;
	}
	
}
