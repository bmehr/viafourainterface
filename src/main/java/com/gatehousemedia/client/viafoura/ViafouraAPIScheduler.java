package com.gatehousemedia.client.viafoura;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import com.gatehousemedia.client.viafoura.constant.PropertyValue;
import com.gatehousemedia.client.viafoura.service.ViafouraHttpService;

@EnableScheduling
@Configuration
public class ViafouraAPIScheduler implements SchedulingConfigurer
{
   @Bean
   public ViafouraHttpService viafouraClient()
   {
      return new ViafouraHttpService();
   }

  @Bean()
  public ThreadPoolTaskScheduler taskScheduler() {
     return new ThreadPoolTaskScheduler();
  }

  @Override
  public void configureTasks(ScheduledTaskRegistrar taskRegistrar)
  {
	  if(ViafouraInterfaceMain.enableSchedule){
	      taskRegistrar.setTaskScheduler(taskScheduler());
	      taskRegistrar.addCronTask(new Runnable()
	      {
	         public void run()
	         {
	        	 viafouraClient().run();
	         }
	      }, PropertyValue.CRON_EXPRESSION); 
	  }
  }
}
