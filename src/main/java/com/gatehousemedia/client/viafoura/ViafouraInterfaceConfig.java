package com.gatehousemedia.client.viafoura;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.gatehousemedia.client.viafoura.constant.PropertyValue;
import com.gatehousemedia.client.viafoura.util.PropertyLoader;


@Configuration
@EnableJpaRepositories
@EnableTransactionManagement
@EnableAutoConfiguration
@EnableScheduling
@ComponentScan(basePackages = "com.gatehousemedia")
public class ViafouraInterfaceConfig {
	
	  @Bean
	  public DataSource dataSource() {
		  BasicDataSource dataSource = new BasicDataSource();
        try{ 
      	  if(PropertyValue.DB_URL == null || "".equals(PropertyValue.DB_URL.trim())){
      		PropertyLoader.loadApplicationProperties();
      	  }
            dataSource.setDriverClassName("com.mysql.jdbc.Driver");
            dataSource.setUrl(PropertyValue.DB_URL);
            dataSource.setUsername(PropertyValue.DB_USR);
            dataSource.setPassword(PropertyValue.DB_PWD);
      }catch(Exception e){
         e.printStackTrace();
         return null;
      }
      return dataSource;
	  }

	  @Bean
	  public EntityManagerFactory entityManagerFactory() {
	    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
	    vendorAdapter.setGenerateDdl(true);

	    LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
	    factory.setJpaVendorAdapter(vendorAdapter);
	    factory.setPackagesToScan("com.gatehousemedia.client.viafoura.db.domain");
	    factory.setDataSource(dataSource());
	    factory.afterPropertiesSet();
	    factory.setJpaProperties(getHibernateProperties());

	    return factory.getObject();
	  }

	  @Bean
	  public PlatformTransactionManager transactionManager() {
	    JpaTransactionManager txManager = new JpaTransactionManager();
	    txManager.setEntityManagerFactory(entityManagerFactory());
	    return txManager;
	  }
	  
	    private Properties getHibernateProperties() {
	        Properties properties = new Properties();
	        
	        properties.put("hibernate.show_sql", "false");
	        properties.put("hibernate.format_sql", "false");
	        properties.put("hibernate.use_sql_comments", "false");
	        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
	        properties.put("spring.jpa.database-platform", "org.hibernate.dialect.MySQLDialect");	        
	        properties.put("log4j.logger.org.hibernate", "fatal");
	        properties.put("log4j.logger.org.springframework", "fatal");
	        properties.put("logging.level.org.hibernate", "fatal");
	//	      properties.put("hibernate.enable_lazy_load_no_trans", "true"); //only if lazy=true
	        //TODO: To configure previous value . please read some article to tune hibernate query - 
	//	      properties.put("hibernate.connection.release_mode", "after_statement");
	        
	        //isolation level
//	        properties.setProperty("hibernate.connection.isolation", String.valueOf(Connection.TRANSACTION_READ_COMMITTED));    
	        
	        
	        return properties;
	    }
}
