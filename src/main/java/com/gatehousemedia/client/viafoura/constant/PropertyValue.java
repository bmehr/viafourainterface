package com.gatehousemedia.client.viafoura.constant;

public class PropertyValue {
	public static String APPLICATION_MODE;
	public static String OS;
	public static String USE_FTP;
	public static String FTPSERVER_ADDRESS;
	public static String FTPSERVER_USR;
	public static String FTPSERVER_PWD;
	public static String FTPSERVER_FOLDERNAME;
	public static String LOCAL_DIRECTORY;
	public static String CLIENT_ID;
	public static String CLIENT_SECRET;
	public static String USE_OVERALL_GATEHOUSE_AGGREGATE_SCOPE;
	public static String CRON_EXPRESSION;
	public static String DB_URL;
	public static String DB_USR;
	public static String DB_PWD;
	
	public static String AWS_ACCESSKEY_ID = "";
	public static String AWS_SECRET_ACCESS_KEY = "";
	public static String AWS_SNS_ARN = "";
}
