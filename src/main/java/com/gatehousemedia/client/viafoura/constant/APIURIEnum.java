package com.gatehousemedia.client.viafoura.constant;

public enum APIURIEnum {
	E_INDICATOR_CLICKS_TOTAL("/engagement/indicator/clicks/total", "e_clicks_total"),
	E_LOADS_JS_TOTAL("/engagement/loads/js/total", "e_js_total"),
	E_NOTIFICATIONS_BROWSER_CLICKED("/engagement/notifications/browser/clicked", "e_browser_clicked"),
	E_NOTIFICATIONS_BROWSER_RECEIVED("/engagement/notifications/browser/received", "e_browser_received"),
	E_TRAY_OPENS_TOTAL("/engagement/tray/opens/total", "e_opens_total"),
	E_VISITORS_ANONYMOUS_TOTAL("/engagement/visitors/anonymous/total", "e_anonymous_total"),
	E_VISITORS_ATTENTION("/engagement/visitors/attention", "e_visitors_attention"),
	E_VISITORS_USER_AGENTS("/engagement/visitors/user_agents", "e_visitors_user_agents"),
	E_VISITORS_VISITS("/engagement/visitors/visits", "e_visitors_visits"),

	U_ANONYMOUS_TOTAL("/users/anonymous/total", "u_anonymous_total"),
	U_COMMENTS("/users/comments", "u_comments"),
	U_DISLIKES("/users/dislikes", "u_dislikes"),
	U_FOLLOWS_PAGES("/users/follows/pages", "u_follows_pages"),
	U_FOLLOWS_USERS("/users/follows/users", "u_follows_users"),
	U_LIKES("/users/likes", "u_likes"),
	U_LOGINS("/users/logins", "u_logins"),
	U_RATINGS("/users/ratings", "u_ratings"),
	U_REGISTERED("/users/registered", "u_registered"),
	U_REGISTERED_TOTAL("/users/registered/total", "u_registered_total"),
	U_SCHEDULE("/users/schedule", "u_schedule"),
	U_TOPICS_SUBSCRIBED("/users/topics/subscribed", "u_topics_subscribed"), //500 Internal Server Error
	U_TOPICS_UNSUBSCRIBED("/users/topics/unsubscribed", "u_topics_unsubscribed"),  // 500 Internal Server Error 
	U_VISITS("/users/visits", "u_visits"); // 504 Gateway Time-out
	
	
	private String url;
	private String fileName;
	  
	APIURIEnum(String url, String fileName){
		this.url = url;
		this.fileName = fileName;
	}

	public String getUrl() {
		return url;
	}

	public String getFileName() {
		return fileName;
	}

}
