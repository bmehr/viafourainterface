package com.gatehousemedia.client.viafoura.constant;

public interface ViafouraInterfaceConstants {
	public String DATE_FORMAT_STANDARD = "dd/MM/yyyy";
	public String DATE_FORMAT_YEAR_FIRST = "yyyy-MM-dd";
	public String DATE_FORMAT_YEAR_FIRST_TIME_MILLISEC = "yyyy-MM-dd HH:mm:ss.SSSSSS";
	public String DATE_FORMAT_YEAR_FIRST_TIME_NO_MILLISEC = "yyyy-MM-dd HH:mm:ss";
	
	public String DEFAULT_TIME_ZONE = "UTC";
	
	public String BESTRIDE_DEFAULT_IMAGE_FORMAT = "png";
	
	public String S3_DEFAULT_SERVER_HOST = "https://s3.amazonaws.com";
	
	public String BESTRIDE_MODE_DEV = "dev";
	public String BESTRIDE_MODE_PROD = "prod";

	
	public String AWS_ACCESSKEY_ID = "aws-accesskey-id";
	public String AWS_SECRET_ACCESS_KEY = "aws-secret-access-key";
	
	public String X_ACCESS_KEY = "X-Access-Key";	
	public String SYSTEM_NAME = "system-name";	
	public String INTERNAL_TOKEN = "INTERNAL";
    
	String USER_SERVICES_BASE_SSL_URL = "user-services-base-ssl-url";
}
