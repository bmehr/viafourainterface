package com.gatehousemedia.client.viafoura.constant;

public interface PropertyKey {
	String APPLICATION_MODE = "application-mode";
	
	String USE_FTP = "use-ftp";
	String FTPSERVER_ADDRESS = "ftpserver-address";
	String FTPSERVER_USR = "ftpserver-usr";
	String FTPSERVER_PWD = "ftpserver-pwd";
	String FTPSERVER_FOLDERNAME = "ftpserver-foldername";
	String FTPSERVER_FOLDERNAME_DEV = "ftpserver-foldername-dev";	
	
	String LOCAL_DIRECTORY = "local-directory";
	String CLIENT_ID = "client-id";
	String CLIENT_SECRET = "client-secret";
	String USE_OVERALL_GATEHOUSE_AGGREGATE_SCOPE = "use-overall-gatehouse-aggregate-scope";
	String CRON_EXPRESSION = "cron-expression";
	
	String DB_URL = "db-url";
	String DB_USR = "db-usr";
	String DB_PWD = "db-pwd";
	String DB_URL_DEV = "db-url-dev";
	String DB_USR_DEV = "db-usr-dev";
	String DB_PWD_DEV = "db-pwd-dev";
	
	String AWS_ACCESSKEY_ID = "aws-accesskey-id";
	String AWS_SECRET_ACCESS_KEY = "aws-secret-access-key";
	String AWS_SNS_ARN = "aws-sns-arn";
	String AWS_SNS_ARN_DEV = "aws-sns-arn-dev";
}
