package com.gatehousemedia.client.viafoura.sns;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.PublishRequest;
import com.gatehousemedia.client.viafoura.constant.PropertyValue;
import com.gatehousemedia.client.viafoura.util.LoggingUtils;



public class SNSHandler {
	private static final String TOPIC_ARN = PropertyValue.AWS_SNS_ARN;
	
	public static void sendSimpleMessage(String message, boolean exception) {
		try{
			AmazonSNS amazonSNS = AmazonSNSClientFactory.getAmazonSNSClient();
	
	        String subjectSend = null;
	        if(exception){
	        	subjectSend = "@ViafouraImport! Failed";
	        }else{
	        	subjectSend = "@ViafouraImport! Good";
	        }		
	        
	        PublishRequest publishRequest = new PublishRequest(TOPIC_ARN, message);
	        publishRequest.setSubject(subjectSend);
	        amazonSNS.publish(publishRequest);
    	}catch(Exception e){
    		e.printStackTrace();
    		LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
    	}	        
    }
	
	public static void sendSimpleMessage(String message, String subjectSend) {
		try{
	        AmazonSNS amazonSNS = AmazonSNSClientFactory.getAmazonSNSClient();
	        
	        PublishRequest publishRequest = new PublishRequest(TOPIC_ARN, message);
	        publishRequest.setSubject(subjectSend);
	        amazonSNS.publish(publishRequest);
    	}catch(Exception e){
    		e.printStackTrace();
    		LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
    	}	        
    }
}
