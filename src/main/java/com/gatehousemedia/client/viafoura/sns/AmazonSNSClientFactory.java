package com.gatehousemedia.client.viafoura.sns;

import java.net.MalformedURLException;

import javax.xml.rpc.ServiceException;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.gatehousemedia.client.viafoura.constant.PropertyValue;

public class AmazonSNSClientFactory {
    private static volatile AmazonSNS amazonSNS = null;

    private AmazonSNSClientFactory(){}

	public static AmazonSNS getAmazonSNSClient() throws ServiceException, MalformedURLException{
        if(amazonSNS == null && !"".equals(PropertyValue.AWS_ACCESSKEY_ID)){
            synchronized (AmazonS3Client.class) {
                // Double check
                if (amazonSNS == null && !"".equals(PropertyValue.AWS_ACCESSKEY_ID)) {
                	AWSCredentials awsCredentials = new BasicAWSCredentials(PropertyValue.AWS_ACCESSKEY_ID, PropertyValue.AWS_SECRET_ACCESS_KEY);
                    amazonSNS = AmazonSNSClient.builder()
    						.withRegion(Regions.US_EAST_1)
    						.withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
    						.build();
                }
            }
        }
        return amazonSNS;
    }
}
